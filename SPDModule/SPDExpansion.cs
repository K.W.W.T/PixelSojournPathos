﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathos
{
  public sealed class SPDExpansion : Expansion
  {
    public SPDExpansion(OfficialCampaign Campaign)
      : base(Campaign, "Pixel Sojourn", "Pixel Sojourn")
    {
      Campaign.AddModule(new SPDModule(Campaign.Codex));
    }
  }
}