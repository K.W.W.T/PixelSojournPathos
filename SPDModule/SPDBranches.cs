﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv;
using Inv.Support;
using Inv.Resource;

namespace Pathos
{
    internal sealed class SPDBranches
    {
        public void CreateBranch()
        {
            if (!SPDDebug.branches.TryGetValue(SPDDebug.currentmap.depth, out int type)) throw new Exception("dict error");
            switch (type)
            {
                case 0: CreateMinesBranch(); break;
                case 1: CreateSokobanBranch(); break;
                case 2: CreateFortBranch(); break;
                case 3: CreateLabyrinthBranch(); break;
                case 4: CreateLostChambersBranch(); break;
                case 5: CreateLairBranch(); break;
                case 6: CreateKingdomBranch(); break;
                case 7: CreateLichTowerBranch(); break;
                case 8: CreateAbyssBranch(); break;
                case 9: CreateMarketBranch(); break;
                default: throw new Exception("out of bound");
                /* BranchInt: 
                * 0 -> Minetown, 1 -> Sokoban, 2 -> Fort, 3 -> Labyrinth
                * 4 -> Chambers, 5 -> Lair, 6 -> Kingdom, 7 -> Tower
                * 9 -> Market, 8 -> Abyss
                */
            }
        }
        public void AssignBranches()
        {
            SPDDebug.branches = new Dictionary<int, int>();
            var LevelInt = new int[10];
            LevelInt[0] = SPDRandom.IntRange(2, 4);
            LevelInt[1] = 5;
            LevelInt[2] = SPDRandom.IntRange(6, 9);
            LevelInt[3] = 10;
            LevelInt[4] = SPDRandom.IntRange(11, 14);
            LevelInt[5] = 15;
            LevelInt[6] = SPDRandom.IntRange(16, 19);
            LevelInt[7] = 20;
            LevelInt[8] = SPDRandom.IntRange(21, 24);
            LevelInt[9] = 25;

            var BranchInt = new Inv.DistinctList<int>();
            for (int i = 0; i < 8; i++){
                BranchInt.Add(i);
            }
            BranchInt.Shuffle();
            BranchInt.Add(8);
            BranchInt.Add(9);

            for (int j = 0; j < 10; j++)
            {
                SPDDebug.branches.Add(LevelInt[j], BranchInt[j]);
            }
            SPDDebug.WriteBranchesList();
            
        }
        private void CreateMinesBranch()
        {
            var MinesName = "Mines";
            var MinesSite = SPDDebug.adventure.World.AddSite(MinesName);
            SPDDebug.currentBranchSite = MinesSite;
            var TownBarrier = SPDDebug.codex.Barriers.stone_wall;
            var TownGround = SPDDebug.codex.Grounds.stone_floor;
            var TownGate = SPDDebug.codex.Gates.wooden_door;
            var GridArray = new[]
            {
                new { Frequency = 12, Name = "Frontier Town", Text = Resources.Specials.Town1Frontier },
                new { Frequency = 12, Name = "Townsquare", Text = Resources.Specials.Town2Square },
                new { Frequency = 12, Name = "Alley Town", Text = Resources.Specials.Town3Alley },
                new { Frequency = 12, Name = "College Town", Text = Resources.Specials.Town4College },
                new { Frequency = 12, Name = "Bustling Town", Text = Resources.Specials.Town5Bustling },
                new { Frequency = 12, Name = "The Bazaar", Text = Resources.Specials.Town6Bazaar },
                new { Frequency = 4, Name = "Orc Town", Text = Resources.Specials.Town7Orcish },
            };
            var TownArray = new[] { GridArray.ToProbability(T => T.Frequency).GetRandom() };
            var TownText = TownArray.First().Text;
            var TownGrid = SPDDebug.LoadSpecialGrid(TownText);
            var MinesGround = SPDDebug.codex.Grounds.dirt_floor;
            var MinesBarrier = SPDDebug.codex.Barriers.cave_wall;
            var MinesLevelDownPortal = SPDDebug.codex.Portals.clay_staircase_down;
            var MinesLevelUpPortal = SPDDebug.codex.Portals.clay_staircase_up;
            for (var Index = 0; Index < (1.d4() - 1).Roll(); Index++) TownGrid.Rotate90Degrees();

            var TownWidth = TownGrid.Width;
            var TownHeight = TownGrid.Height;

            var TownMap = SPDDebug.adventure.World.AddMap("MineTown", TownWidth, TownHeight);
            TownMap.SetDifficulty(SPDDebug.currentmap.depth);
            SetDifficultyGroup();

            TownMap.SetAtmosphere(SPDDebug.codex.Atmospheres.civilisation);
            var TownLevel = MinesSite.AddLevel(1, TownMap);

            var TownParty = SPDDebug.generator.NewParty(Leader: null);
            var DeferList = new Inv.DistinctList<Action>();

            var TownShopProbability = SPDDebug.codex.Shops.List.Where(S => !SPDDebug.adventure.Abolition || !S.SellsOnlyAbolitionCandidates()).ToProbability(M => M.Rarity); // TODO: Rewrite shop type
            var ShrineProbability = SPDDebug.codex.Shrines.List.ToProbability(S => S.Rarity);

            var GuardArray = new Character[9];
            Square AboveSquare = null;
            Square BelowSquare = null;

            for (var Column = 0; Column < TownGrid.Width; Column++)
            {
                for (var Row = 0; Row < TownGrid.Height; Row++)
                {
                    var TownSymbol = TownGrid[Column, Row];
                    var TownSquare = TownMap[Column, Row];
                    

                    switch (TownSymbol)
                    {
                        case '<':
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            TownSquare.SetLit(true);
                            AboveSquare = TownSquare;
                            break;

                        case '>':
                            BelowSquare = TownSquare;
                            SPDDebug.generator.PlaceFloor(TownSquare, MinesGround);
                            SPDDebug.generator.PlacePassage(BelowSquare, MinesLevelDownPortal, Destination: null);
                            break;

                        case '.':
                            // room floor.
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            TownSquare.SetLit(true);
                            break;

                        case '#':
                            // corridor floor.
                            SPDDebug.generator.PlaceFloor(TownSquare, MinesGround);
                            TownSquare.SetLit(true);
                            break;

                        case '-':
                        case '|':
                            // walls.
                            SPDDebug.generator.PlaceSolidWall(TownSquare, TownBarrier, WallSegment.Cross);
                            TownSquare.SetLit(true);
                            break;

                        case '+':
                            // door.
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            TownSquare.SetLit(true);

                            if (TownGate == null)
                            {
                                SPDDebug.generator.PlaceIllusionaryWall(TownSquare, TownBarrier, WallSegment.Cross);
                            }
                            else
                            {
                                SPDDebug.generator.PlaceClosedHorizontalDoor(TownSquare, TownGate, TownBarrier);
                            }
                            break;

                        case '=':
                            // workbench.
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            TownSquare.SetLit(true);

                            SPDDebug.generator.PlaceFixture(TownSquare, SPDDebug.codex.Features.workbench);
                            break;

                        case '{':
                            // fountain.
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            TownSquare.SetLit(true);

                            SPDDebug.generator.PlaceFixture(TownSquare, SPDDebug.codex.Features.fountain);
                            break;

                        case '}':
                            // tree.
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            SPDDebug.generator.PlaceSolidWall(TownSquare, SPDDebug.codex.Barriers.tree, WallSegment.Pillar);
                            TownSquare.SetLit(true);
                            break;

                        case '_':
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            TownSquare.SetLit(true);

                            var TownShrine = ShrineProbability.GetRandom();

                            SPDDebug.generator.PlaceShrine(TownSquare, TownShrine);
                            RecruitParty(TownSquare, TownParty);

                            DeferList.Add(() =>
                            {
                                var ShrineZone = TownMap.AddZone();
                                ShrineZone.AddRegion(TownSquare.FindBoundary());

                                ShrineZone.InsertTrigger().AddSchedule(Delay.Zero, SPDDebug.codex.Tricks.VisitShrineArray[TownShrine.Index]).SetTarget(TownSquare);
                            });

                            break;

                        case '@':
                        case '%':
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            TownSquare.SetLit(true);

                            var TownShop = TownShopProbability.RemoveRandomOrNull();

                            if (TownShop != null)
                            {
                                if (TownSymbol == '@')
                                {
                                    PlaceShop(TownSquare, Orcs: false);
                                    RecruitParty(TownSquare, TownParty);
                                }
                                else
                                {
                                    PlaceShop(TownSquare, Orcs: true);
                                    SPDDebug.generator.CorpseSquare(TownSquare);

                                    // murdered by orcs.
                                    DeferList.Add(() =>
                                    {
                                        var ShopParty = SPDDebug.generator.PlaceHorde(SPDDebug.codex.Hordes.orc, TownSquare.MinimumDifficulty(null), TownSquare.MaximumDifficulty(null), () => SPDDebug.generator.CanPlaceCharacter(TownSquare) ? TownSquare : SPDDebug.generator.ConcentricFindSquare(TownSquare, 3));
                                        if (ShopParty == null)
                                            SPDDebug.generator.PlaceCharacter(TownSquare, SPDDebug.codex.Entities.orc_grunt);
                                    });
                                }
                            }
                            break;

                        case '&':
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            TownSquare.SetLit(true);

                            SPDDebug.generator.PlaceShrine(TownSquare, ShrineProbability.GetRandom());
                            SPDDebug.generator.CorpseSquare(TownSquare);

                            DeferList.Add(() =>
                            {
                                var ShrineParty = SPDDebug.generator.PlaceHorde(SPDDebug.codex.Hordes.orc, TownSquare.MinimumDifficulty(null), TownSquare.MaximumDifficulty(null), () => SPDDebug.generator.CanPlaceCharacter(TownSquare) ? TownSquare : SPDDebug.generator.ConcentricFindSquare(TownSquare, 3));
                                if (ShrineParty == null)
                                    SPDDebug.generator.PlaceCharacter(TownSquare, SPDDebug.codex.Entities.orc_grunt);
                            });
                            break;

                        case 'f':
                        case 'k':
                        case 'l':
                        case 'n':
                        case 'w':
                        case 'y':
                        case 'G':
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            TownSquare.SetLit(true);
                            CreateMonster(TownSquare);
                            break;
                        case '\\':
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            TownSquare.SetLit(true);

                            SPDDebug.generator.PlaceFixture(TownSquare, SPDDebug.codex.Features.bed);
                            break;

                        case ' ':
                            // void.
                            TownSquare.SetFloor(null);
                            TownSquare.SetLit(false);
                            break;

                        default:
                            SPDDebug.generator.PlaceFloor(TownSquare, TownGround);
                            TownSquare.SetLit(true);
                            break;
                    }
                }
            }
            void CreateMonster(Square DestSquare)
            {
                if (SPDRandom.Int(5) < 4){ //80% chance to generate themed monsters.
                    switch (DifficultyGroup)
                    {
                        case 1:
                            CreateLevelOneMonster(DestSquare);
                            break;
                        case 2:
                            switch (SPDRandom.Int(5)){
                                case 0: case 1: PlaceCharacter(DestSquare, 1, 10, E => E.Kind == SPDDebug.codex.Kinds.gnome); break;
                                case 2: case 3: PlaceCharacter(DestSquare, 1, 10, E => E.Kind == SPDDebug.codex.Kinds.kobold); break;
                                case 4: PlaceCharacter(DestSquare, 1, 8, E => E.Kind == SPDDebug.codex.Kinds.dragon); break;}
                            if (DestSquare.Character != null) SPDDebug.generator.PromoteCharacter(DestSquare.Character, SPDRandom.IntRange(9, 11) - DestSquare.Character.Level);
                            break;
                        case 3: case 4: case 5:
                            switch (SPDRandom.Int(4)){
                                case 0: PlaceCharacter(DestSquare, 1, DifficultyGroup * 4 + 2, E => E.Kind == SPDDebug.codex.Kinds.gnome); break;
                                case 1: PlaceCharacter(DestSquare, 1, DifficultyGroup * 4 + 2, E => E.Kind == SPDDebug.codex.Kinds.kobold); break;
                                case 2: PlaceCharacter(DestSquare, 1, DifficultyGroup * 4, E => E.Kind == SPDDebug.codex.Kinds.dragon); break;
                                case 3: PlaceCharacter(DestSquare, 1, DifficultyGroup * 4 + 2, E => E.Kind == SPDDebug.codex.Kinds.dwarf); break;}
                            if (DestSquare.Character != null) SPDDebug.generator.PromoteCharacter(DestSquare.Character, DifficultyGroup * 4 - 2 + SPDRandom.Int(4) - DestSquare.Character.Level);
                            break;
                        case 6:
                            switch (SPDRandom.Int(6)){
                                case 0: PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.gnome); break;
                                case 1: PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.kobold); break;
                                case 2: PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.dwarf); break;
                                case 3: PlaceCharacter(DestSquare, 10, 25, E => E.Kind == SPDDebug.codex.Kinds.dragon); break;
                                case 4: PlaceCharacter(DestSquare, 1, 28, E => E.Kind == SPDDebug.codex.Kinds.giant); break;
                                case 5: PlaceCharacter(DestSquare, 1, 27, E => E.Kind == SPDDebug.codex.Kinds.gnoll); break;}
                            if (DestSquare.Character != null) SPDDebug.generator.PromoteCharacter(DestSquare.Character, 22 + SPDRandom.Int(5) - DestSquare.Character.Level);
                            break;
                        case 7:
                            switch (SPDRandom.Int(6)){
                                case 0: PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.dwarf); break;
                                case 1: PlaceCharacter(DestSquare, 20, 32, E => E.Kind == SPDDebug.codex.Kinds.dragon); break;
                                case 2: PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.giant); break;
                                case 3: PlaceCharacter(DestSquare, 20, 32, E => E.Kind == SPDDebug.codex.Kinds.quadruped); break;
                                case 4: PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.gnoll); break;
                                case 5: PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.troll); break;}
                            if (DestSquare.Character != null) SPDDebug.generator.PromoteCharacter(DestSquare.Character, 26 + SPDRandom.Int(6) - DestSquare.Character.Level);
                            break;
                        case 8:
                            switch (SPDRandom.Int(5)){
                                case 0: PlaceCharacter(DestSquare, 25, 36, E => E.Kind == SPDDebug.codex.Kinds.dragon); break;
                                case 1: PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.giant); break;
                                case 2: PlaceCharacter(DestSquare, 20, 36, E => E.Kind == SPDDebug.codex.Kinds.quadruped); break;
                                case 3: PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.gnoll); break;
                                case 4: PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.troll); break;
                            }
                            if (DestSquare.Character != null) SPDDebug.generator.PromoteCharacter(DestSquare.Character, 26 + SPDRandom.Int(6) - DestSquare.Character.Level);
                            break;
                    }
                } else {
                    if (DifficultyGroup == 1) CreateLevelOneMonster(DestSquare);
                    else
                    {
                        SPDDebug.generator.PlaceCharacter(DestSquare);
                        SPDDebug.generator.PromoteCharacter(DestSquare.Character, DifficultyGroup * 4 - 2 + SPDRandom.Int(4) - DestSquare.Character.Level);
                    }
                }
                if (DestSquare.Character != null) SnoozeCharacter(DestSquare.Character);
            }
            void CreateBoss(Square Square)
            {
                Character KingChar;
                switch (DifficultyGroup)
                {
                    case 1:
                        CreateLevelOneMonster(Square);
                        KingChar = Square.Character;
                        if (KingChar != null)
                        {
                            SPDDebug.generator.PromoteCharacter(Square.Character, 5);

                            var Properties = SPDDebug.codex.Properties;
                            var Elements = SPDDebug.codex.Elements;

                            KingChar.AcquireTalent(Properties.free_action, Properties.polymorph_control, Properties.slippery);
                            KingChar.SetResistance(Elements.magical, 100);

                            KingChar.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(Square, SPDDebug.gamelist.BranchTreasure(10)));
                        }
                        break;
                    case 2: case 3: case 4: case 5: case 6:
                        switch (SPDRandom.Int(3)){
                            case 0: SPDDebug.generator.PlaceCharacter(Square, SPDDebug.codex.Entities.gnome_king); break;
                            case 1: SPDDebug.generator.PlaceCharacter(Square, SPDDebug.codex.Entities.kobold_king); break;
                            case 2: SPDDebug.generator.PlaceCharacter(Square, SPDDebug.codex.Entities.dwarf_king); break;}
                        KingChar = Square.Character;
                        if (KingChar != null)
                        {
                            SPDDebug.generator.PromoteCharacter(Square.Character, DifficultyGroup * 4 - 2 + SPDRandom.Int(4) - KingChar.Level);

                            var Properties = SPDDebug.codex.Properties;
                            var Elements = SPDDebug.codex.Elements;

                            KingChar.AcquireTalent(Properties.free_action, Properties.polymorph_control, Properties.slippery);
                            KingChar.SetResistance(Elements.magical, 100);
                            if (DifficultyGroup > 2)
                            {
                                KingChar.SetResistance(Elements.fire, 100);
                                KingChar.SetResistance(Elements.poison, 100);
                                KingChar.SetResistance(Elements.cold, 100);
                                KingChar.SetResistance(Elements.sleep, 100);
                            }
                            if (DifficultyGroup > 4)
                            {
                                KingChar.SetResistance(Elements.drain, 100);
                                KingChar.SetResistance(Elements.shock, 100);
                                KingChar.SetResistance(Elements.water, 100);
                            }

                            KingChar.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(Square, SPDDebug.gamelist.BranchTreasure(10)));
                        }
                        break;
                    case 7:
                        switch (SPDRandom.Int(3)){
                            case 0: SPDDebug.generator.PlaceCharacter(Square, SPDDebug.codex.Entities.dwarf_king); break;
                            case 1: SPDDebug.generator.PlaceCharacter(Square, SPDDebug.codex.Entities.giant_king); break;
                            case 2: SPDDebug.generator.PlaceCharacter(Square, SPDDebug.codex.Entities.juggernaut); break;}
                        KingChar = Square.Character;
                        if (KingChar != null)
                        {
                            SPDDebug.generator.PromoteCharacter(Square.Character, 26 + SPDRandom.Int(4) - KingChar.Level);

                            var Properties = SPDDebug.codex.Properties;
                            var Elements = SPDDebug.codex.Elements;

                            KingChar.AcquireTalent(Properties.free_action, Properties.polymorph_control, Properties.slippery);
                            KingChar.SetResistance(Elements.magical, 100);
                            KingChar.SetResistance(Elements.fire, 100);
                            KingChar.SetResistance(Elements.poison, 100);
                            KingChar.SetResistance(Elements.cold, 100);
                            KingChar.SetResistance(Elements.sleep, 100);
                            KingChar.SetResistance(Elements.drain, 100);
                            KingChar.SetResistance(Elements.shock, 100);
                            KingChar.SetResistance(Elements.water, 100);

                            KingChar.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(Square, SPDDebug.gamelist.BranchTreasure(10)));
                            KingChar.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(Square, SPDDebug.gamelist.BranchTreasure(10)));
                        }
                        break;
                    case 8:
                        switch (SPDRandom.Int(4))
                        {
                            case 0: SPDDebug.generator.PlaceCharacter(Square, SPDDebug.codex.Entities.dragon_king); break;
                            case 1: SPDDebug.generator.PlaceCharacter(Square, SPDDebug.codex.Entities.giant_king); break;
                            case 2: SPDDebug.generator.PlaceCharacter(Square, SPDDebug.codex.Entities.giant_shoggoth); break;
                            case 3: SPDDebug.generator.PlaceCharacter(Square, SPDDebug.codex.Entities.juggernaut); break;
                        }
                        KingChar = Square.Character;
                        if (KingChar != null)
                        {
                            SPDDebug.generator.PromoteCharacter(Square.Character, 26 + SPDRandom.Int(4) - KingChar.Level);

                            var Properties = SPDDebug.codex.Properties;
                            var Elements = SPDDebug.codex.Elements;

                            KingChar.AcquireTalent(Properties.free_action, Properties.polymorph_control, Properties.slippery);
                            KingChar.SetResistance(Elements.magical, 100);
                            KingChar.SetResistance(Elements.fire, 100);
                            KingChar.SetResistance(Elements.poison, 100);
                            KingChar.SetResistance(Elements.cold, 100);
                            KingChar.SetResistance(Elements.sleep, 100);
                            KingChar.SetResistance(Elements.drain, 100);
                            KingChar.SetResistance(Elements.shock, 100);
                            KingChar.SetResistance(Elements.water, 100);
                            KingChar.SetResistance(Elements.acid, 100);
                            KingChar.SetResistance(Elements.petrify, 100);
                            KingChar.SetResistance(Elements.force, 50);

                            KingChar.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(Square, SPDDebug.gamelist.BranchTreasure(10)));
                            KingChar.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(Square, SPDDebug.gamelist.BranchTreasure(10)));
                        }
                        break;
                }
            }
            TownLevel.SetTransitions(AboveSquare, BelowSquare);

            foreach (var Square in TownMap.GetSquares(TownMap.Region))
            {
                if (Square.IsEmpty() && Square.GetAdjacentSquares().Any(S => S.Floor != null && S.Wall == null))
                    SPDDebug.generator.PlaceSolidWall(Square, MinesBarrier, WallSegment.Cross);
            }

            foreach (var Defer in DeferList)
                Defer();

            SPDDebug.generator.RepairVoid(TownMap, TownMap.Region);
            SPDDebug.generator.RepairWalls(TownMap, TownMap.Region);
            SPDDebug.generator.RepairDoors(TownMap, TownMap.Region);
            SPDDebug.generator.RepairBridges(TownMap, TownMap.Region);
            SPDDebug.generator.RepairZones(TownMap, TownMap.Region);

            var LevelArray = new[]
            {
                new { Number = 1, Text = Resources.Specials.Mines1 },
                new { Number = 2, Text = Resources.Specials.Mines2 },
                new { Number = 3, Text = Resources.Specials.Mines3 },
                new { Number = 4, Text = Resources.Specials.Mines4 },
                new { Number = 5, Text = Resources.Specials.Mines5 },
            };
            Square NextLevelSquare = null;
            foreach (var Level in LevelArray)
            {
                var LevelMapName = "Mines " + Level.Number;

                var MinesGrid = SPDDebug.generator.LoadSpecialGrid(Level.Text);

                // rotate 0-3 times (0/90/180/270 degrees).
                for (var Index = 0; Index < (1.d4() - 1).Roll(); Index++)
                    MinesGrid.Rotate90Degrees();

                var MinesMap = SPDDebug.generator.Adventure.World.AddMap(LevelMapName, MinesGrid.Width, MinesGrid.Height);
                MinesMap.SetDifficulty(SPDDebug.currentmap.depth);
                MinesMap.SetAtmosphere(SPDDebug.codex.Atmospheres.cavern);

                var MinesLevel = MinesSite.AddLevel(Level.Number + 1, MinesMap);

                Square DownLevelSquare = null;

                for (var Column = 0; Column < MinesGrid.Width; Column++)
                {
                    for (var Row = 0; Row < MinesGrid.Height; Row++)
                    {
                        var MinesSymbol = MinesGrid[Column, Row];
                        var MinesSquare = MinesMap[Column, Row];

                        switch (MinesSymbol)
                        {
                            case ' ':
                                // void.
                                break;

                            case '.':
                                // room floor.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                MinesSquare.SetLit(true);
                                break;

                            case '#':
                                // corridor floor.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                MinesSquare.SetLit(true);
                                break;

                            case '-':
                            case '|':
                                // walls.
                                SPDDebug.generator.PlaceSolidWall(MinesSquare, MinesBarrier, WallSegment.Cross);
                                MinesSquare.SetLit(true);
                                break;

                            case '`':
                                // can't dig through this wall.
                                SPDDebug.generator.PlacePermanentWall(MinesSquare, MinesBarrier, WallSegment.Cross);
                                MinesSquare.SetLit(true);
                                break;

                            case '~':
                                // locked door to Under levels staircase.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                SPDDebug.generator.PlaceLockedHorizontalDoor(MinesSquare, SPDDebug.codex.Gates.crystal_door, MinesBarrier);
                                MinesSquare.SetLit(true);
                                break;

                            case '+':
                                // door.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                SPDDebug.generator.PlaceIllusionaryWall(MinesSquare, MinesBarrier, WallSegment.Cross);
                                MinesSquare.SetLit(true);
                                break;

                            case 'S':
                                // secret door.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                SPDDebug.generator.PlaceIllusionaryWall(MinesSquare, MinesBarrier, WallSegment.Cross);
                                MinesSquare.SetLit(true);
                                break;

                            case 'D':
                                // locked secret door.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                SPDDebug.generator.PlaceIllusionaryWall(MinesSquare, MinesBarrier, WallSegment.Cross);
                                MinesSquare.SetLit(true);
                                break;

                            case 'H':
                                // maybe floor, maybe wall.
                                if (Chance.OneIn2.Hit())
                                    SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                else
                                    SPDDebug.generator.PlaceSolidWall(MinesSquare, MinesBarrier, WallSegment.Cross);
                                MinesSquare.SetLit(true);
                                break;

                            case '<':
                                // up stair.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                MinesSquare.SetLit(true);

                                var DestinationSquare = NextLevelSquare ?? BelowSquare;

                                DestinationSquare.Passage.SetDestination(MinesSquare);
                                SPDDebug.generator.PlacePassage(MinesSquare, MinesLevelUpPortal, DestinationSquare);

                                MinesLevel.SetTransitions(MinesSquare, MinesLevel.DownSquare);
                                break;

                            case '>':
                                // down stair.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);

                                DownLevelSquare = MinesSquare;

                                SPDDebug.generator.PlacePassage(DownLevelSquare, MinesLevelDownPortal, Destination: null);

                                MinesLevel.SetTransitions(MinesLevel.UpSquare, MinesSquare);
                                
                                MinesSquare.SetLit(true);
                                break;

                            case 'g':
                            case 'l':
                            case 't':
                            case 'm':
                            case 'z':
                            case 'w':
                            case 'k':
                            case 'U':
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                MinesSquare.SetLit(true);
                                if(SPDRandom.Int(5) < 3) CreateMonster(MinesSquare); // 60% chance to generate a monster
                                break;
                            case 'Z': //Zoo
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                MinesSquare.SetLit(true);
                                if (SPDRandom.Int(3) == 0) CreateMonster(MinesSquare);
                                break;

                            case '\\':
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                SPDDebug.generator.PlaceFixture(MinesSquare, SPDDebug.codex.Features.throne);
                                CreateBoss(MinesSquare);
                                MinesSquare.SetLit(true);
                                break;

                            case '0':
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                MinesSquare.SetLit(true);
                                break;

                            case '*':
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                MinesSquare.SetLit(true);
                                if (SPDRandom.Int(10) == 0)
                                {
                                    if (SPDRandom.Int(4) == 0) SPDDebug.generator.PlaceSpecificAsset(MinesSquare, SPDDebug.gamelist.RandomBook());
                                    else SPDDebug.generator.PlaceSpecificAsset(MinesSquare, SPDDebug.gamelist.BranchTreasure(DifficultyGroup));
                                }
                                break;

                            case '!':
                            case '?':
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                MinesSquare.SetLit(true);
                                break;

                            case '{':
                                // fountain.                
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                SPDDebug.generator.PlaceFixture(MinesSquare, SPDDebug.codex.Features.fountain);
                                MinesSquare.SetLit(true);
                                break;

                            case '_':
                                // altar.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                SPDDebug.generator.PlaceFixture(MinesSquare, SPDDebug.codex.Features.altar);
                                MinesSquare.SetLit(true);
                                break;

                            case '&':
                                // grave.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                SPDDebug.generator.PlaceFixture(MinesSquare, SPDDebug.codex.Features.grave);
                                MinesSquare.SetLit(true);
                                break;

                            case '=':
                                // workbench.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                SPDDebug.generator.PlaceFixture(MinesSquare, SPDDebug.codex.Features.workbench);
                                MinesSquare.SetLit(true);
                                break;

                            case '^':
                                // portal back to the dungeon.
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                MinesLevel.SetTransitions(MinesLevel.UpSquare, MinesSquare);
                                MinesSquare.SetLit(true);
                                break;

                            default:
                                SPDDebug.generator.PlaceFloor(MinesSquare, MinesGround);
                                MinesSquare.SetLit(true);
                                break;
                        }
                    }
                }

                SPDDebug.generator.RepairVoid(MinesMap, MinesMap.Region);
                SPDDebug.generator.RepairWalls(MinesMap, MinesMap.Region);
                SPDDebug.generator.RepairDoors(MinesMap, MinesMap.Region);
                SPDDebug.generator.RepairZones(MinesMap, MinesMap.Region);

                foreach (var MinesZone in MinesMap.Zones)
                {
                    //if (Chance.OneIn3.Hit())
                        //CreateRoomDetails
                }

                NextLevelSquare = DownLevelSquare;
            }
        }
        private void CreateLostChambersBranch()
        {
            var ChambersSite = SPDDebug.generator.Adventure.World.AddSite("Lost Chambers");
            SPDDebug.currentBranchSite = ChambersSite;
            Assert.Disable(); //TODO: analyze this

            var ResourceFile = Resources.Quests.Chambers;

            var Quest = SPDDebug.generator.ImportQuest(ResourceFile.GetBuffer());

            var QuestSite = Quest.World.Sites.Single();
            var QuestStart = Quest.World.Start;
            var QuestDifficulty = SPDDebug.currentmap.depth;
            SetDifficultyGroup();

            const int QuestColumns = 5;
            const int QuestRows = 5;
            const int ChamberSize = 17;
            const int ChamberMargin = -1;
            const int TotalWidth = QuestColumns * (ChamberSize + ChamberMargin) - ChamberMargin;
            const int TotalHeight = QuestRows * (ChamberSize + ChamberMargin) - ChamberMargin;

            var PointList = new DistinctList<Inv.Point>(QuestColumns * QuestRows);

            var PatternArray = new string[QuestRows]
            {
            "#####",
            "## ##",
            "# * #",
            "## ##",
            "#####",
            };

            for (var Y = 0; Y < QuestRows; Y++)
            {
                for (var X = 0; X < QuestColumns; X++)
                {
                    if (PatternArray[Y][X] == '#')
                        PointList.Add(new Inv.Point(X, Y));
                }
            }

            PointList.Shuffle();
            PointList.Add(new Point(2, 2));

            var BossCharacter = (Character)null;

            var ChamberMap = SPDDebug.generator.Adventure.World.AddMap("Lost Chambers", TotalWidth, TotalHeight);
            ChamberMap.SetDifficulty(QuestDifficulty);
            ChamberMap.SetAtmosphere(SPDDebug.codex.Atmospheres.nether);
            ChamberMap.SetTerminal(true);

            var ChamberLevel = ChambersSite.AddLevel(1, ChamberMap);

            var ConversionDictionary = new Dictionary<Square, Square>(TotalWidth * TotalHeight);

            // transfer from the quest to the actual map.
            var PointIndex = 0;

            //Make changes for PS first
            foreach (var QuestLevel in QuestSite.Levels)
            {
                var TargetPoint = PointList[PointIndex++];
                var TargetX = (TargetPoint.X * ChamberSize) + (ChamberMargin * TargetPoint.X);
                var TargetY = (TargetPoint.Y * ChamberSize) + (ChamberMargin * TargetPoint.Y);

                for (var SourceY = 0; SourceY < ChamberSize; SourceY++)
                {
                    for (var SourceX = 0; SourceX < ChamberSize; SourceX++)
                    {
                        var QuestSquare = QuestLevel.Map[SourceX, SourceY];
                        var Codex = SPDDebug.codex;
                        
                        if (!QuestSquare.IsVoid() && QuestSquare.Floor?.Ground != null)
                        {
                            switch (QuestLevel.Index)
                            {
                                case 1: if (QuestSquare.Floor.Ground == Codex.Grounds.moss) { if (SPDRandom.Int(5) == 0) PlaceMonster(QuestSquare, 0); QuestSquare.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor); } break;
                                case 2: if (QuestSquare.Floor.Ground == Codex.Grounds.obsidian_floor && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(40) == 0) PlaceMonster(QuestSquare, 1); break;
                                case 3:
                                    {
                                        if (QuestSquare.Floor.Ground == Codex.Grounds.hive_floor && QuestSquare.Trap == null && SPDRandom.Int(4) == 0) PlaceMonster(QuestSquare, 21);
                                        if (QuestSquare.Floor.Ground == Codex.Grounds.ice && QuestSquare.Trap == null && SPDRandom.Int(4) == 0) PlaceMonster(QuestSquare, 22);
                                        if (QuestSquare.Floor.Ground == Codex.Grounds.grass && QuestSquare.Trap == null && SPDRandom.Int(4) == 0) PlaceMonster(QuestSquare, 23);
                                        if (QuestSquare.Floor.Ground == Codex.Grounds.lava && QuestSquare.Trap == null && SPDRandom.Int(4) == 0) PlaceMonster(QuestSquare, 24);
                                        break;
                                    }
                                case 4: if (QuestSquare.Floor.Ground == Codex.Grounds.obsidian_floor && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(40) == 0) PlaceMonster(QuestSquare, 3); break;
                                case 5: if (QuestSquare.Floor.Ground == Codex.Grounds.obsidian_floor && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(30) == 0) PlaceMonster(QuestSquare, 4); break;
                                case 6: if (QuestSquare.Floor.Ground == Codex.Grounds.obsidian_floor && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(30) == 0) PlaceMonster(QuestSquare, 5); break;
                                case 7:
                                    if (QuestSquare.Floor.Ground == Codex.Grounds.obsidian_floor && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(12) == 0) PlaceMonster(QuestSquare, 61);
                                    if (QuestSquare.Floor.Ground == Codex.Grounds.water && QuestSquare.Trap == null && SPDRandom.Int(10) == 0) PlaceMonster(QuestSquare, 62);
                                    break;
                                case 8: if (QuestSquare.Floor.Ground == Codex.Grounds.obsidian_floor && QuestSquare.Wall == null && QuestSquare.Boulder == null && SPDRandom.Int(30) == 0) PlaceMonster(QuestSquare, 7); break;
                                case 9: if (QuestSquare.Floor.Ground == Codex.Grounds.obsidian_floor && QuestSquare.Wall == null && SPDRandom.Int(8) == 0) PlaceMonster(QuestSquare, 8); break;
                                case 10: if (QuestSquare.Floor.Ground == Codex.Grounds.obsidian_floor && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(20) == 0) PlaceMonster(QuestSquare, 9); break;
                                case 11: if (QuestSquare.Floor.Ground == Codex.Grounds.obsidian_floor && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(40) == 0) PlaceMonster(QuestSquare, 10); break;
                                case 12: if (QuestSquare.Floor.Ground == Codex.Grounds.obsidian_floor && QuestSquare.Wall == null && QuestSquare.Trap == null) PlaceMonster(QuestSquare, 11); break;
                                case 14: if (QuestSquare.Floor.Ground == Codex.Grounds.lava && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(30) == 0) PlaceMonster(QuestSquare, 13); break;
                                case 15: if (QuestSquare.Floor.Ground == Codex.Grounds.ice && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(15) == 0) PlaceMonster(QuestSquare, 14); break;
                                case 16: if (QuestSquare.Floor.Ground == Codex.Grounds.dirt_floor && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(6) == 0) PlaceMonster(QuestSquare, 15); break;
                                case 17: if (QuestSquare.Floor.Ground == Codex.Grounds.grass && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(25) == 0) PlaceMonster(QuestSquare, 16); break;
                                case 19: if (QuestSquare.Floor.Ground == Codex.Grounds.obsidian_floor && QuestSquare.Wall == null && QuestSquare.Trap == null && SPDRandom.Int(8) == 0) PlaceMonster(QuestSquare, 18); break;
                                case 21: if (QuestSquare.Floor.Ground == Codex.Grounds.wooden_floor && QuestSquare.Wall == null && QuestSquare.Trap == null) { PlaceMonster(QuestSquare, 19); QuestSquare.Floor.SetGround(Codex.Grounds.obsidian_floor); }
                                    if (QuestSquare.Floor.Ground == Codex.Grounds.metal_floor && QuestSquare.Wall == null && QuestSquare.Trap == null) { PlaceMonster(QuestSquare, 20); QuestSquare.Floor.SetGround(Codex.Grounds.obsidian_floor); }
                                    break;
                            }
                            if (QuestSquare.Floor.Ground == Codex.Grounds.sand)
                            {
                                QuestSquare.Floor.SetGround(Codex.Grounds.obsidian_floor);
                                if (SPDRandom.Int(4) == 0)
                                {
                                    if (SPDRandom.Int(4) == 0) SPDDebug.generator.PlaceSpecificAsset(QuestSquare, SPDDebug.gamelist.RandomBook());
                                    else SPDDebug.generator.PlaceSpecificAsset(QuestSquare, SPDDebug.gamelist.BranchTreasure(DifficultyGroup));
                                }
                            }
                            QuestSquare.SetLit(true);
                        }
                    }
                }
            }

            PointIndex = 0;
            foreach (var QuestLevel in QuestSite.Levels)
            {

                QuestLevel.Map.RotateRandomDegrees();

                var TargetPoint = PointList[PointIndex++];
                var TargetX = (TargetPoint.X * ChamberSize) + (ChamberMargin * TargetPoint.X);
                var TargetY = (TargetPoint.Y * ChamberSize) + (ChamberMargin * TargetPoint.Y);

                for (var SourceY = 0; SourceY < ChamberSize; SourceY++)
                {
                    for (var SourceX = 0; SourceX < ChamberSize; SourceX++)
                    {
                        var QuestSquare = QuestLevel.Map[SourceX, SourceY];
                        if (!QuestSquare.IsVoid())
                        {
                            var ChamberSquare = ChamberMap[TargetX + SourceX, TargetY + SourceY];
                            if (ChamberSquare.Wall == null) // ChamberMargin causes an overlap.
                                ChamberSquare.Transfer(QuestSquare);

                            if (ChamberSquare.Floor != null)
                                ConversionDictionary.Add(QuestSquare, ChamberSquare);
                        }
                    }
                }
            }

            SPDDebug.generator.BuildMap(ChamberMap);

            void ResidentArea(Square ResidentSquare, Trick ResidentTrick)
            {
                // make the immediate squares part of the resident zone.
                var ChamberZone = ChamberMap.AddZone();
                ChamberZone.ForceSquare(ResidentSquare);
                foreach (var AdjacentSquare in ResidentSquare.GetAdjacentSquares())
                    ChamberZone.ForceSquare(AdjacentSquare);

                // all residents start with psychosis in the Lost Chambers.
                var ResidentCharacter = ResidentSquare.Character;
                if (ResidentCharacter != null)
                {
                    SPDDebug.generator.PunishCharacter(ResidentCharacter, SPDDebug.codex.Punishments.psychosis);

                    ChamberZone.InsertTrigger().AddSchedule(Delay.Zero, ResidentTrick).SetTarget(ResidentSquare);
                }
            }

            // convert passage destinations to the assigned square.
            foreach (var ChamberSquare in ChamberMap.GetSquares())
            {
                if (ChamberSquare.Fixture?.Feature == SPDDebug.codex.Features.stall)
                {
                    var Shop = SPDDebug.codex.Shops.general_store;
                    if (Shop != null)
                    {
                        PlaceShop(ChamberSquare, false);

                        ResidentArea(ChamberSquare, SPDDebug.codex.Tricks.VisitShopArray[Shop.Index]);
                    }
                }
                else if (ChamberSquare.Fixture?.Feature == SPDDebug.codex.Features.altar)
                {
                    var Shrine = SPDDebug.codex.Shrines.dark_sepulchre;
                    if (Shrine != null)
                    {
                        SPDDebug.generator.PlaceShrine(ChamberSquare, Shrine);

                        ResidentArea(ChamberSquare, SPDDebug.codex.Tricks.VisitShrineArray[Shrine.Index]);
                    }
                }

                // passage connections.
                if (ChamberSquare.Passage != null && ChamberSquare.Passage.Destination != null)
                    ChamberSquare.Passage.SetDestination(ConversionDictionary[ChamberSquare.Passage.Destination]);

                // no-dig.
                if (ChamberSquare.Wall != null && ChamberSquare.Wall.IsPhysical())
                    ChamberSquare.Wall.SetStructure(WallStructure.Permanent);

            }

            if (BossCharacter != null)
            {

                if (BossCharacter != null)
                {
                    SPDDebug.generator.PromoteCharacter(BossCharacter, 5);

                    var Properties = SPDDebug.codex.Properties;
                    var Elements = SPDDebug.codex.Elements;

                    BossCharacter.AcquireTalent(Properties.polymorph_control, Properties.slippery, Properties.clarity, Properties.free_action);
                    BossCharacter.SetResistance(Elements.poison, 100);
                    BossCharacter.SetResistance(Elements.shock, 100);
                    BossCharacter.SetResistance(Elements.cold, 100);

                    // master competency in all skills.
                    foreach (var Competency in BossCharacter.Competencies)
                        Competency.Set(SPDDebug.codex.Qualifications.master);

                    SPDDebug.generator.AcquireArtifact(BossCharacter.Square, BossCharacter, SPDDebug.codex.Qualifications.master);
                }
            }

            // no-teleport.
            foreach (var ChamberZone in ChamberMap.Zones)
                ChamberZone.SetRestricted(true);

            var ChamberStart = ConversionDictionary[QuestStart];
            ChamberLevel.SetTransitions(ChamberStart, ChamberStart);

            void PlaceMonster(Square Square, int Index)
            {
                var Codex = SPDDebug.codex;
                var Generator = SPDDebug.generator;
                switch (DifficultyGroup)
                {
                    case 1: CreateLevelOneMonster(Square); break;
                    case 2: switch (Index)
                        {
                            case 0: Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); break;
                            case 1: if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.yeoman);
                                else Generator.PlaceCharacter(Square, Codex.Entities.yeoman_warder);
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 21: Generator.PlaceCharacter(Square, Codex.Entities.killer_bee); break;
                            case 22:
                                switch (SPDRandom.Int(3)){
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_vortex); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.yeti); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.frost_sphere); break;
                                } break;
                            case 23:
                                switch (SPDRandom.Int(4)){
                                    case 0: Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.beast
                                    && E != Codex.Entities.jabberwock && E != Codex.Entities.vorpal_jabberwock && E != Codex.Entities.minotaur
                                    && E != Codex.Entities.xorn && E != Codex.Entities.yeti && E != Codex.Entities.zruty); break;
                                    case 1: Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.bird
                                    && E != Codex.Entities.cockatrice && E != Codex.Entities.chickatrice && E != Codex.Entities.pyrolisk); break;
                                    case 2: Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.quadruped
                                    && E != Codex.Entities.catoblepas && E != Codex.Entities.juggernaut && E != Codex.Entities.squealer); break;
                                    case 3: Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.horse
                                    && E != Codex.Entities.pegasus && E != Codex.Entities.nightmare); break;
                                } break;
                            case 24: 
                                switch (SPDRandom.Int(4)){
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.hellbat); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.pyrolisk); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.flame_sphere); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.hellrat); break;
                                } break;
                            case 3: switch (SPDRandom.Int(3)) //mercenary
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.acolyte); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.artisan); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.student); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true);
                                break;
                            case 4: switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.guard); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.acolyte); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.intern); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.student); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.guide); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 5: if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.incubus);
                                else Generator.PlaceCharacter(Square, Codex.Entities.succubus); break; // TODO: maybe tweak ATK/DEF
                            case 61: switch (SPDRandom.Int(4)) {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.guard); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.water_seeker); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.mugger); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.yeoman); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.frost_seeker); break;
                                } //guard/seeker/mugger/yeoman
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 62: Generator.PlaceCharacter(Square, 1, 11, E => E.Kind == Codex.Kinds.marine); break; //marine
                            case 7: switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.earth_seeker); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.mugger); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.merchant); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.neanderthal); break;
                                } //earth_seeker/mugger/merchant/neanderthal
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 8: switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceSpecificAsset(Square, Codex.Items.egg); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.bad_egg); break;
                                    case 2: case 3: switch (SPDRandom.Int(3))
                                        {
                                            case 0: Generator.PlaceCharacter(Square, Codex.Entities.giant_spider); break;
                                            case 1: Generator.PlaceCharacter(Square, Codex.Entities.cave_spider); break;
                                            case 2: Generator.PlaceCharacter(Square, Codex.Entities.barking_spider); break;
                                        } break;
                                } break; //spiders
                            case 9: if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.nurse);
                                else Generator.PlaceCharacter(Square, Codex.Entities.genetic_engineer);
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 10: Generator.PlaceCharacter(Square, Codex.Entities.stone_giant); break;
                            case 11: if (SPDRandom.Int(80) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.yeoman); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(40) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.exterminator); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else Generator.PlaceCharacter(Square, Codex.Entities.giant_cockroach); break;
                            case 13: //strong fire monsters
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.hellbat); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.pyrolisk); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.flame_sphere); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.hellrat); break;
                                }
                                break;
                            case 14: //strong ice monsters
                                switch (SPDRandom.Int(3)){
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_vortex); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.yeti); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.frost_sphere); break;
                                } break;
                            case 15: if (SPDRandom.Int(8) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.embalmer); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else Generator.PlaceCharacter(Square, 1, 11, E => E.Kind == Codex.Kinds.zombie); break;
                            case 16: //forest neutral
                                switch (SPDRandom.Int(7)) {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.forest_centaur); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.wood_nymph); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.lichen); break;
                                    case 3: case 4: Generator.PlaceCharacter(Square, 1, 12, E => E.Kind == Codex.Kinds.elf); break;
                                    case 5: case 6: Generator.PlaceCharacter(Square, 1, 9, E => E.Kind == Codex.Kinds.horse); break;
                                } if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 18: if (SPDRandom.Int(5) == 0) Generator.PlaceCharacter(Square, Codex.Entities.doppelganger);
                                else Generator.PlaceCharacter(Square, 6, 11, E => E.Kind == Codex.Kinds.mimic); break;
                            case 19: Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor); break;
                            case 20: Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor);
                                if (Square.Character != null) BossCharacter = Square.Character; break;
                        } if (Square.Character != null && Square.Character.Entity != Codex.Entities.giant_cockroach) Generator.PromoteCharacter(Square.Character, 9 + SPDRandom.Int(2) - Square.Character.Level);
                        break;
                    case 3:
                        switch (Index)
                        {
                            case 0: Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); break;
                            case 1:
                                if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.yeoman);
                                else Generator.PlaceCharacter(Square, Codex.Entities.yeoman_warder);
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 21: Generator.PlaceCharacter(Square, Codex.Entities.killer_bee); break;
                            case 22:
                                switch (SPDRandom.Int(6))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_vortex); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.yeti); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.frost_sphere); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.ice_devil); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.winter_wolf_cub); break;
                                    case 5: Generator.PlaceCharacter(Square, Codex.Entities.snow_golem); break;
                                }
                                break;
                            case 23:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.beast
                                    && E != Codex.Entities.jabberwock && E != Codex.Entities.vorpal_jabberwock && E != Codex.Entities.minotaur
                                    && E != Codex.Entities.xorn && E != Codex.Entities.yeti && E != Codex.Entities.zruty); break;
                                    case 1: Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.bird
                                    && E != Codex.Entities.cockatrice && E != Codex.Entities.chickatrice && E != Codex.Entities.pyrolisk); break;
                                    case 2: Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.quadruped
                                    && E != Codex.Entities.catoblepas && E != Codex.Entities.juggernaut && E != Codex.Entities.squealer); break;
                                    case 3: Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.horse
                                    && E != Codex.Entities.pegasus && E != Codex.Entities.nightmare); break;
                                }
                                break;
                            case 24: 
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.hellbat); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.pyrolisk); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.flame_sphere); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.hellrat); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.hell_hound_pup); break;
                                }
                                break;
                            case 3:
                                switch (SPDRandom.Int(5)) //mercenary
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.acolyte); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.artisan); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.student); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.apprentice); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.attendant); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true);
                                break;
                            case 4:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.guard); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.acolyte); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.intern); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.student); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.guide); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 5:
                                if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.incubus);
                                else Generator.PlaceCharacter(Square, Codex.Entities.succubus); break;
                            case 61:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.guard); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.water_seeker); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.mugger); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.yeoman); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.frost_seeker); break;
                                } //guard/seeker/mugger/yeoman
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 62: Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.marine); break; //marine
                            case 7:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.earth_seeker); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.mugger); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.merchant); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.neanderthal); break;
                                } //earth_seeker/mugger/merchant/neanderthal
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 8:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceSpecificAsset(Square, Codex.Items.egg); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.bad_egg); break;
                                    case 2:
                                    case 3:
                                        switch (SPDRandom.Int(3))
                                        {
                                            case 0: Generator.PlaceCharacter(Square, Codex.Entities.giant_spider); break;
                                            case 1: Generator.PlaceCharacter(Square, Codex.Entities.cave_spider); break;
                                            case 2: Generator.PlaceCharacter(Square, Codex.Entities.barking_spider); break;
                                        }
                                        break;
                                }
                                break; //spiders
                            case 9:
                                if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.nurse);
                                else Generator.PlaceCharacter(Square, Codex.Entities.genetic_engineer);
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 10:
                                switch (SPDRandom.Int(3))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.stone_giant); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.hill_giant); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.umber_hulk); break;
                                }
                                break;
                            case 11:
                                if (SPDRandom.Int(80) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.yeoman); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(40) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.exterminator); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(60) == 0) Generator.PlaceCharacter(Square, Codex.Entities.soldier_ant);
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.giant_ant);
                                else Generator.PlaceCharacter(Square, Codex.Entities.giant_cockroach); break;
                            case 13: //strong fire monsters
                                switch (SPDRandom.Int(4))
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.pyrolisk); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.hellcat); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.flame_sphere); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.hell_hound_pup); break;
                                }
                                break;
                            case 14: //strong ice monsters
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_vortex); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.ice_devil); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.winter_wolf_cub); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.snow_golem); break;
                                }
                                break;
                            case 15:
                                if (SPDRandom.Int(8) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.embalmer); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.zombie); break;
                            case 16: //forest neutral
                                switch (SPDRandom.Int(7))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.forest_centaur); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.wood_nymph); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.lichen); break;
                                    case 3: case 4: Generator.PlaceCharacter(Square, 1, 12, E => E.Kind == Codex.Kinds.elf); break;
                                    case 5: case 6: Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.horse); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 18:
                                if (SPDRandom.Int(5) == 0) Generator.PlaceCharacter(Square, Codex.Entities.doppelganger);
                                else Generator.PlaceCharacter(Square, 6, 14, E => E.Kind == Codex.Kinds.mimic); break;
                            case 19: Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor); break;
                            case 20:
                                Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor);
                                if (Square.Character != null) BossCharacter = Square.Character; break;
                        } if (Square.Character != null && Square.Character.Entity != Codex.Entities.giant_cockroach) Generator.PromoteCharacter(Square.Character, 11 + SPDRandom.Int(3) - Square.Character.Level);
                        break;
                    case 4:
                        switch (Index)
                        {
                            case 0: Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); break;
                            case 1:
                                switch (SPDRandom.Int(3))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.yeoman); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.yeoman_warder); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.page); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 21:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: case 4:  Generator.PlaceCharacter(Square, Codex.Entities.killer_bee); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.yellow_jacket); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.giant_wasp); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.black_wasp); break;
                                }
                                break;
                            case 22:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_vortex); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.yeti); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.ice_devil); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.winter_wolf_cub); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.snow_golem); break;
                                }
                                break;
                            case 23:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0:
                                        Generator.PlaceCharacter(Square, 1, 18, E => E.Kind == Codex.Kinds.beast
                                && E != Codex.Entities.jabberwock && E != Codex.Entities.vorpal_jabberwock && E != Codex.Entities.minotaur
                                && E != Codex.Entities.xorn && E != Codex.Entities.yeti && E != Codex.Entities.zruty); break;
                                    case 1:
                                        Generator.PlaceCharacter(Square, 1, 18, E => E.Kind == Codex.Kinds.bird
                                && E != Codex.Entities.cockatrice && E != Codex.Entities.chickatrice && E != Codex.Entities.pyrolisk); break;
                                    case 2:
                                        Generator.PlaceCharacter(Square, 1, 18, E => E.Kind == Codex.Kinds.quadruped
                                && E != Codex.Entities.catoblepas && E != Codex.Entities.juggernaut && E != Codex.Entities.squealer); break;
                                    case 3:
                                        Generator.PlaceCharacter(Square, 1, 18, E => E.Kind == Codex.Kinds.horse
                                && E != Codex.Entities.pegasus && E != Codex.Entities.nightmare); break;
                                }
                                break;
                            case 24:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.hellbat); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.pyrolisk); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.hellcat); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.hell_hound_pup); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.fire_elemental); break;
                                }
                                break;
                            case 3:
                                switch (SPDRandom.Int(4)) //mercenary
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.artisan); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.apprentice); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.attendant); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.abbot); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true);
                                break;
                            case 4:
                                switch (SPDRandom.Int(2))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.guard); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.guide); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 5:
                                if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.incubus);
                                else Generator.PlaceCharacter(Square, Codex.Entities.succubus); break;
                            case 61:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.guard); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.water_binder); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.thug); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.yeoman); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.frost_binder); break;
                                } //guard/seeker/mugger/yeoman
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 62: Generator.PlaceCharacter(Square, 1, 18, E => E.Kind == Codex.Kinds.marine); break; //marine
                            case 7:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.earth_binder); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.mugger); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.merchant); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.neanderthal); break;
                                } //earth_seeker/mugger/merchant/neanderthal
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 8:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceSpecificAsset(Square, Codex.Items.egg); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.bad_egg); break;
                                    case 2:
                                    case 3:
                                        switch (SPDRandom.Int(4))
                                        {
                                            case 0: Generator.PlaceCharacter(Square, Codex.Entities.giant_spider); break;
                                            case 1: Generator.PlaceCharacter(Square, Codex.Entities.cave_spider); break;
                                            case 2: Generator.PlaceCharacter(Square, Codex.Entities.barking_spider); break;
                                            case 3: Generator.PlaceCharacter(Square, Codex.Entities.phase_spider); break;
                                        }
                                        break;
                                }
                                break; //spiders
                            case 9:
                                if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.nurse);
                                else Generator.PlaceCharacter(Square, Codex.Entities.genetic_engineer);
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 10:
                                switch (SPDRandom.Int(3))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.stone_giant); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.hill_giant); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.umber_hulk); break;
                                }
                                break;
                            case 11:
                                if (SPDRandom.Int(80) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.yeoman); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(40) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.exterminator); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(60) == 0) Generator.PlaceCharacter(Square, Codex.Entities.soldier_ant);
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.giant_ant);
                                else Generator.PlaceCharacter(Square, Codex.Entities.giant_cockroach); break;
                            case 13: //strong fire monsters
                                switch (SPDRandom.Int(4))
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.pyrolisk); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.hellcat); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.fire_elemental); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.hell_hound_pup); break;
                                }
                                break;
                            case 14: //strong ice monsters
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_vortex); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.ice_devil); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.winter_wolf_cub); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.snow_golem); break;
                                }
                                break;
                            case 15:
                                if (SPDRandom.Int(8) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.embalmer); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else Generator.PlaceCharacter(Square, 1, 18, E => E.Kind == Codex.Kinds.zombie); break;
                            case 16: //forest neutral
                                switch (SPDRandom.Int(7))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.forest_centaur); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.wood_nymph); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.lichen); break;
                                    case 3: case 4: Generator.PlaceCharacter(Square, 1, 18, E => E.Kind == Codex.Kinds.elf); break;
                                    case 5: case 6: Generator.PlaceCharacter(Square, 1, 14, E => E.Kind == Codex.Kinds.horse); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 18:
                                if (SPDRandom.Int(5) == 0) Generator.PlaceCharacter(Square, Codex.Entities.doppelganger);
                                else Generator.PlaceCharacter(Square, 6, 18, E => E.Kind == Codex.Kinds.mimic); break;
                            case 19: Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor); break;
                            case 20:
                                Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor);
                                if (Square.Character != null) BossCharacter = Square.Character; break;
                        }
                        if (Square.Character != null && Square.Character.Entity != Codex.Entities.giant_cockroach) Generator.PromoteCharacter(Square.Character, 15 + SPDRandom.Int(3) - Square.Character.Level);
                        break;
                    case 5:
                        switch (Index)
                        {
                            case 0: if (SPDRandom.Int(3) == 0) Generator.PlaceCharacter(Square, Codex.Entities.master_mind_flayer);
                                else Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); break;
                            case 1:
                                switch (SPDRandom.Int(3))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.yeoman); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.tracker); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 21:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.killer_bee); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.yellow_jacket); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.giant_wasp); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.black_wasp); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.queen_bee); break;
                                }
                                break;
                            case 22:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_vortex); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.young_white_dragon); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.ice_devil); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.winter_wolf); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.snow_golem); break;
                                }
                                break;
                            case 23:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0:
                                        Generator.PlaceCharacter(Square, 1, 23, E => E.Kind == Codex.Kinds.beast
                                && E != Codex.Entities.jabberwock && E != Codex.Entities.vorpal_jabberwock && E != Codex.Entities.minotaur
                                && E != Codex.Entities.xorn && E != Codex.Entities.yeti && E != Codex.Entities.zruty); break;
                                    case 1:
                                        Generator.PlaceCharacter(Square, 1, 18, E => E.Kind == Codex.Kinds.bird
                                && E != Codex.Entities.cockatrice && E != Codex.Entities.chickatrice && E != Codex.Entities.pyrolisk); break;
                                    case 2:
                                        Generator.PlaceCharacter(Square, 1, 23, E => E.Kind == Codex.Kinds.quadruped
                                && E != Codex.Entities.catoblepas && E != Codex.Entities.juggernaut && E != Codex.Entities.squealer); break;
                                    case 3:
                                        Generator.PlaceCharacter(Square, 1, 18, E => E.Kind == Codex.Kinds.horse
                                && E != Codex.Entities.pegasus && E != Codex.Entities.nightmare); break;
                                }
                                break;
                            case 24:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.young_red_dragon); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.fire_elemental); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.hellcat); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.hell_hound); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.fire_giant); break;
                                }
                                break;
                            case 3:
                                switch (SPDRandom.Int(4)) //mercenary
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.artisan); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.fiend); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.roshi); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.abbot); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true);
                                break;
                            case 4:
                                switch (SPDRandom.Int(2))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.guard); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 5:
                                if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.incubus);
                                else Generator.PlaceCharacter(Square, Codex.Entities.succubus); break;
                            case 61:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.guard); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.water_binder); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.thug); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.frost_binder); break;
                                } //guard/seeker/mugger/yeoman
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 62: Generator.PlaceCharacter(Square, 1, 23, E => E.Kind == Codex.Kinds.marine); break; //marine
                            case 7:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.earth_binder); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.thug); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.merchant); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.neanderthal); break;
                                } //earth_seeker/mugger/merchant/neanderthal
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 8:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceSpecificAsset(Square, Codex.Items.egg); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.bad_egg); break;
                                    case 2:
                                    case 3:
                                        switch (SPDRandom.Int(4))
                                        {
                                            case 0: Generator.PlaceCharacter(Square, Codex.Entities.spiderwere); break;
                                            case 1: Generator.PlaceCharacter(Square, Codex.Entities.recluse_spider); break;
                                            case 2: Generator.PlaceCharacter(Square, Codex.Entities.werespider); break;
                                            case 3: Generator.PlaceCharacter(Square, Codex.Entities.phase_spider); break;
                                        }
                                        break;
                                }
                                break; //spiders
                            case 9:
                                if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.nurse);
                                else Generator.PlaceCharacter(Square, Codex.Entities.genetic_engineer);
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 10:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.rock_troll); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.stone_golem); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.umber_hulk); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.clay_golem); break;
                                }
                                break;
                            case 11:
                                if (SPDRandom.Int(80) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.yeoman); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(40) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.exterminator); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(60) == 0) Generator.PlaceCharacter(Square, Codex.Entities.soldier_ant);
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.giant_louse);
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.assassin_bug);
                                else Generator.PlaceCharacter(Square, Codex.Entities.giant_cockroach); break;
                            case 13: //strong fire monsters
                                switch (SPDRandom.Int(5))
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.hell_hound); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.hellcat); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.fire_elemental); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.fire_giant); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.fire_vortex); break;
                                }
                                break;
                            case 14: //strong ice monsters
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_vortex); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.ice_elemental); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.winter_wolf); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.snow_golem); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.young_white_dragon); break;
                                }
                                break;
                            case 15:
                                if (SPDRandom.Int(8) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.embalmer); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else { if(SPDRandom.Int(2)==0) Generator.PlaceCharacter(Square, 1, 22, E => E.Kind == Codex.Kinds.mummy);
                                    else Generator.PlaceCharacter(Square, 1, 22, E => E.Kind == Codex.Kinds.zombie); } break;
                            case 16: //forest neutral
                                switch (SPDRandom.Int(7))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.forest_centaur); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.wood_nymph); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.lichen); break;
                                    case 3: case 4: Generator.PlaceCharacter(Square, 1, 23, E => E.Kind == Codex.Kinds.elf); break;
                                    case 5: case 6: Generator.PlaceCharacter(Square, 1, 20, E => E.Kind == Codex.Kinds.horse); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 18:
                                if (SPDRandom.Int(5) == 0) Generator.PlaceCharacter(Square, Codex.Entities.doppelganger);
                                else Generator.PlaceCharacter(Square, 6, 23, E => E.Kind == Codex.Kinds.mimic); break;
                            case 19: Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor); break;
                            case 20:
                                Generator.PlaceCharacter(Square, Codex.Entities.master_mind_flayer); Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor);
                                if (Square.Character != null) BossCharacter = Square.Character; break;
                        }
                        if (Square.Character != null && Square.Character.Entity != Codex.Entities.giant_cockroach) Generator.PromoteCharacter(Square.Character, 18 + SPDRandom.Int(4) - Square.Character.Level);
                        break;
                    case 6:
                        switch (Index)
                        {
                            case 0:
                                if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.master_mind_flayer);
                                else Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); break;
                            case 1:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.chief_yeoman_warder); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.tracker); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.chieftain); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 21:
                                switch (SPDRandom.Int(4))
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.yellow_jacket); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.giant_wasp); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.black_wasp); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.queen_bee); break;
                                }
                                break;
                            case 22:
                                switch (SPDRandom.Int(3))
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.young_white_dragon); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.winter_wolf); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.snow_golem); break;
                                }
                                break;
                            case 23:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0:
                                        Generator.PlaceCharacter(Square, 1, 26, E => E.Kind == Codex.Kinds.beast
                                && E != Codex.Entities.jabberwock && E != Codex.Entities.vorpal_jabberwock && E != Codex.Entities.minotaur
                                && E != Codex.Entities.xorn && E != Codex.Entities.yeti && E != Codex.Entities.zruty); break;
                                    case 1:
                                        Generator.PlaceCharacter(Square, 15, 26, E => E.Kind == Codex.Kinds.bird
                                && E != Codex.Entities.cockatrice && E != Codex.Entities.chickatrice && E != Codex.Entities.pyrolisk); break;
                                    case 2:
                                        Generator.PlaceCharacter(Square, 10, 26, E => E.Kind == Codex.Kinds.quadruped
                                && E != Codex.Entities.catoblepas && E != Codex.Entities.juggernaut && E != Codex.Entities.squealer); break;
                                    case 3:
                                        Generator.PlaceCharacter(Square, 5, 26, E => E.Kind == Codex.Kinds.horse
                                && E != Codex.Entities.pegasus && E != Codex.Entities.nightmare); break;
                                }
                                break;
                            case 24:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.young_red_dragon); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.hellcat); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.hell_hound); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.fire_giant); break;
                                }
                                break;
                            case 3:
                                switch (SPDRandom.Int(4)) //mercenary
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.shifter); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.fiend); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.roshi); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.abbot); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true);
                                break;
                            case 4:
                                switch (SPDRandom.Int(2))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.chieftain); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 5:
                                if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.incubus);
                                else Generator.PlaceCharacter(Square, Codex.Entities.succubus); break;
                            case 61:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.chieftain); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.water_maker); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.thug); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.frost_maker); break;
                                } //guard/seeker/mugger/yeoman
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 62: Generator.PlaceCharacter(Square, 12, 26, E => E.Kind == Codex.Kinds.marine); break; //marine
                            case 7:
                                switch (SPDRandom.Int(3))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.earth_maker); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.thug); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.merchant); break;
                                } //earth_seeker/mugger/merchant/neanderthal
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 8:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceSpecificAsset(Square, Codex.Items.egg); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.bad_egg); break;
                                    case 2:
                                    case 3:
                                        switch (SPDRandom.Int(4))
                                        {
                                            case 0: Generator.PlaceCharacter(Square, Codex.Entities.spiderwere); break;
                                            case 1: Generator.PlaceCharacter(Square, Codex.Entities.recluse_spider); break;
                                            case 2: Generator.PlaceCharacter(Square, Codex.Entities.werespider); break;
                                            case 3: Generator.PlaceCharacter(Square, Codex.Entities.phase_spider); break;
                                        }
                                        break;
                                }
                                break; //spiders
                            case 9:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.nurse); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.genetic_engineer); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.quantum_mechanic); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.transmuter); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.flesh_golem); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 10:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.rock_troll); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.stone_golem); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.iron_golem); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.clay_golem); break;
                                }
                                break;
                            case 11:
                                if (SPDRandom.Int(80) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.chief_yeoman_warder); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(40) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.exterminator); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(60) == 0) Generator.PlaceCharacter(Square, Codex.Entities.soldier_ant);
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.giant_louse);
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.assassin_bug);
                                else Generator.PlaceCharacter(Square, Codex.Entities.giant_cockroach); break;
                            case 13: //strong fire monsters
                                switch (SPDRandom.Int(5))
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.hell_hound); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.hellcat); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.adult_red_dragon); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.fire_giant); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.fire_vortex); break;
                                }
                                break;
                            case 14: //strong ice monsters
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_vortex); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.ice_elemental); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.winter_wolf); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.snow_golem); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.adult_white_dragon); break;
                                }
                                break;
                            case 15:
                                if (SPDRandom.Int(8) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.embalmer); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else
                                {
                                    if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, 10, 27, E => E.Kind == Codex.Kinds.mummy);
                                    else Generator.PlaceCharacter(Square, 10, 27, E => E.Kind == Codex.Kinds.zombie);
                                }
                                break;
                            case 16: //forest neutral
                                switch (SPDRandom.Int(6))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.forest_centaur); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.wood_nymph); break;
                                    case 3: case 4: Generator.PlaceCharacter(Square, 1, 23, E => E.Kind == Codex.Kinds.elf); break;
                                    case 5: case 2: Generator.PlaceCharacter(Square, 1, 20, E => E.Kind == Codex.Kinds.horse); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 18:
                                if (SPDRandom.Int(5) == 0) Generator.PlaceCharacter(Square, Codex.Entities.doppelganger);
                                else Generator.PlaceCharacter(Square, 6, 27, E => E.Kind == Codex.Kinds.mimic); break;
                            case 19: Generator.PlaceCharacter(Square, Codex.Entities.mind_flayer); Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor); break;
                            case 20:
                                Generator.PlaceCharacter(Square, Codex.Entities.master_mind_flayer); Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor);
                                if (Square.Character != null) BossCharacter = Square.Character; break;
                        }
                        if (Square.Character != null && Square.Character.Entity != Codex.Entities.giant_cockroach) Generator.PromoteCharacter(Square.Character, 21 + SPDRandom.Int(5) - Square.Character.Level);
                        break;
                    case 7:
                        switch (Index)
                        {
                            case 0: switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, E => E.Kind == Codex.Kinds.flayer); break;
                                    case 1: Generator.PlaceCharacter(Square, 15, 32, E => E.Kind == Codex.Kinds.angel); break;
                                    case 2: Generator.PlaceCharacter(Square, 20, 32, E => E.Kind == Codex.Kinds.demon); break;
                                    case 3: Generator.PlaceCharacter(Square, E => E.Kind == Codex.Kinds.devourer); break;
                                } break;
                            case 1:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.chief_yeoman_warder); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.high_priest); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.chieftain); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.tracker); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 21:
                                switch (SPDRandom.Int(2))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.migo_queen); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.queen_bee); break;
                                }
                                break;
                            case 22:
                                switch (SPDRandom.Int(3))
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.adult_white_dragon); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.winter_wolf); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.ice_elemental); break;
                                }
                                break;
                            case 23:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0:
                                        Generator.PlaceCharacter(Square, 15, 32, E => E.Kind == Codex.Kinds.beast
                                && E != Codex.Entities.jabberwock && E != Codex.Entities.vorpal_jabberwock && E != Codex.Entities.minotaur
                                && E != Codex.Entities.xorn && E != Codex.Entities.yeti && E != Codex.Entities.zruty); break;
                                    case 2:
                                        Generator.PlaceCharacter(Square, 10, 32, E => E.Kind == Codex.Kinds.quadruped
                                && E != Codex.Entities.catoblepas && E != Codex.Entities.juggernaut && E != Codex.Entities.squealer); break;
                                    case 1:
                                        Generator.PlaceCharacter(Square, 5, 32, E => E.Kind == Codex.Kinds.horse
                                && E != Codex.Entities.pegasus && E != Codex.Entities.nightmare); break;
                                }
                                break;
                            case 24:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.adult_red_dragon); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.salamander); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.ruby_golem); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.fire_vortex); break;
                                }
                                break;
                            case 3:
                                switch (SPDRandom.Int(4)) //mercenary
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.shifter); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.fiend); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.undead_slayer); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.abbot); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true);
                                break;
                            case 4:
                                switch (SPDRandom.Int(2))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.chieftain); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 5:
                                if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.incubus);
                                else Generator.PlaceCharacter(Square, Codex.Entities.succubus); break;
                            case 61:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.chieftain); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.water_maker); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.undead_slayer); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.frost_maker); break;
                                } //guard/seeker/mugger/yeoman
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 62: Generator.PlaceCharacter(Square, 12, 32, E => E.Kind == Codex.Kinds.marine); break; //marine
                            case 7:
                                switch (SPDRandom.Int(3))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.earth_maker); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.thug); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.merchant); break;
                                } //earth_seeker/mugger/merchant/neanderthal
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 8:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceSpecificAsset(Square, Codex.Items.egg); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.bad_egg); break;
                                    case 2:
                                    case 3:
                                        switch (SPDRandom.Int(3))
                                        {
                                            case 0: Generator.PlaceCharacter(Square, Codex.Entities.spiderwere); break;
                                            case 2: Generator.PlaceCharacter(Square, Codex.Entities.werespider); break;
                                            case 1: Generator.PlaceCharacter(Square, Codex.Entities.spider_queen); break;
                                        }
                                        break;
                                }
                                break; //spiders
                            case 9:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.nurse); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.genetic_engineer); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.quantum_mechanic); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.transmuter); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.flesh_golem); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 10:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.rock_troll); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.stone_golem); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.iron_golem); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.clay_golem); break;
                                }
                                break;
                            case 11:
                                if (SPDRandom.Int(80) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.chief_yeoman_warder); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(40) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.exterminator); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.killer_beetle);
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.spitting_beetle);
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.giant_beetle);
                                else Generator.PlaceCharacter(Square, Codex.Entities.giant_cockroach); break;
                            case 13: //strong fire monsters
                                switch (SPDRandom.Int(3))
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.adult_red_dragon); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.ruby_golem); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.fire_vortex); break;
                                }
                                break;
                            case 14: //strong ice monsters
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_elemental); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.adult_white_dragon); break;
                                }
                                break;
                            case 15:
                                if (SPDRandom.Int(8) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.occultist); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else
                                {
                                    if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, 10, 32, E => E.Kind == Codex.Kinds.mummy);
                                    else Generator.PlaceCharacter(Square, 10, 32, E => E.Kind == Codex.Kinds.zombie);
                                }
                                break;
                            case 16: //forest neutral
                                switch (SPDRandom.Int(6))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.treant); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.wood_nymph); break;
                                    case 3: case 4: Generator.PlaceCharacter(Square, 1, 32, E => E.Kind == Codex.Kinds.elf); break;
                                    case 5: case 2: Generator.PlaceCharacter(Square, 1, 20, E => E.Kind == Codex.Kinds.horse); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 18:
                                if (SPDRandom.Int(5) == 0) Generator.PlaceCharacter(Square, Codex.Entities.doppelganger);
                                else Generator.PlaceCharacter(Square, 6, 32, E => E.Kind == Codex.Kinds.mimic); break;
                            case 19:
                                switch (SPDRandom.Int(6))
                                {
                                    case 0: case 1: Generator.PlaceCharacter(Square, E => E.Kind == Codex.Kinds.flayer); break;
                                    case 2: Generator.PlaceCharacter(Square, 15, 32, E => E.Kind == Codex.Kinds.angel); break;
                                    case 3: Generator.PlaceCharacter(Square, 20, 32, E => E.Kind == Codex.Kinds.demon); break;
                                    case 4: case 5: Generator.PlaceCharacter(Square, E => E.Kind == Codex.Kinds.devourer); break;
                                }
                                Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor); break;
                            case 20:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.master_mind_flayer); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.disenchanter); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.banshee); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.dracolich); break;
                                }
                                
                                Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor);
                                if (Square.Character != null) BossCharacter = Square.Character; break;
                        }
                        if (Square.Character != null && Square.Character.Entity != Codex.Entities.giant_cockroach) Generator.PromoteCharacter(Square.Character, 21 + SPDRandom.Int(5) - Square.Character.Level);
                        break;
                    case 8:
                        switch (Index)
                        {
                            case 0:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, E => E.Kind == Codex.Kinds.flayer); break;
                                    case 1: Generator.PlaceCharacter(Square, 15, 40, E => E.Kind == Codex.Kinds.angel); break;
                                    case 2: Generator.PlaceCharacter(Square, 20, 40, E => E.Kind == Codex.Kinds.demon); break;
                                    case 3: Generator.PlaceCharacter(Square, E => E.Kind == Codex.Kinds.devourer); break;
                                }
                                break;
                            case 1:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.chief_yeoman_warder); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.high_priest); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.chieftain); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 21:
                                switch (SPDRandom.Int(2))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.migo_queen); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.queen_bee); break;
                                }
                                break;
                            case 22:
                                switch (SPDRandom.Int(2))
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.ancient_white_dragon); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_elemental); break;
                                }
                                break;
                            case 23:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0:
                                        Generator.PlaceCharacter(Square, 15, 40, E => E.Kind == Codex.Kinds.beast
                                && E != Codex.Entities.jabberwock && E != Codex.Entities.vorpal_jabberwock && E != Codex.Entities.minotaur
                                && E != Codex.Entities.xorn && E != Codex.Entities.yeti && E != Codex.Entities.zruty); break;
                                    case 1:
                                        Generator.PlaceCharacter(Square, 10, 40, E => E.Kind == Codex.Kinds.quadruped
                                && E != Codex.Entities.catoblepas && E != Codex.Entities.juggernaut && E != Codex.Entities.squealer); break;
                                }
                                break;
                            case 24:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ancient_red_dragon); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.salamander); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.ruby_golem); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.fire_vortex); break;
                                }
                                break;
                            case 3:
                                switch (SPDRandom.Int(3)) //mercenary
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.transmuter); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.fiend); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.undead_slayer); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true);
                                break;
                            case 4:
                                switch (SPDRandom.Int(2))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.chieftain); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 5:
                                if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, Codex.Entities.incubus);
                                else Generator.PlaceCharacter(Square, Codex.Entities.succubus); break;
                            case 61:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.chieftain); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.water_maker); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.undead_slayer); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.warrior); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.frost_maker); break;
                                } //guard/seeker/mugger/yeoman
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 62: Generator.PlaceCharacter(Square, 12, 40, E => E.Kind == Codex.Kinds.marine); break; //marine
                            case 7:
                                switch (SPDRandom.Int(3))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.earth_maker); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.thug); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.merchant); break;
                                } //earth_seeker/mugger/merchant/neanderthal
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 8:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceSpecificAsset(Square, Codex.Items.egg); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.bad_egg); break;
                                    case 2:
                                    case 3:
                                        switch (SPDRandom.Int(3))
                                        {
                                            case 0: Generator.PlaceCharacter(Square, Codex.Entities.spiderwere); break;
                                            case 2: Generator.PlaceCharacter(Square, Codex.Entities.werespider); break;
                                            case 1: Generator.PlaceCharacter(Square, Codex.Entities.spider_queen); break;
                                        }
                                        break;
                                }
                                break; //spiders
                            case 9:
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.nurse); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.genetic_engineer); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.quantum_mechanic); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.transmuter); break;
                                    case 4: Generator.PlaceCharacter(Square, Codex.Entities.flesh_golem); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 10:
                                switch (SPDRandom.Int(3))
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.stone_golem); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.iron_golem); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.clay_golem); break;
                                }
                                break;
                            case 11:
                                if (SPDRandom.Int(80) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.chief_yeoman_warder); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(40) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.exterminator); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.killer_beetle);
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.spitting_beetle);
                                else if (SPDRandom.Int(40) == 0) Generator.PlaceCharacter(Square, Codex.Entities.giant_beetle);
                                else Generator.PlaceCharacter(Square, Codex.Entities.giant_cockroach); break;
                            case 13: //strong fire monsters
                                switch (SPDRandom.Int(3))
                                {
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.ancient_red_dragon); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.ruby_golem); break;
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.fire_vortex); break;
                                }
                                break;
                            case 14: //strong ice monsters
                                switch (SPDRandom.Int(5))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.ice_elemental); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.ancient_white_dragon); break;
                                }
                                break;
                            case 15:
                                if (SPDRandom.Int(8) == 0) { Generator.PlaceCharacter(Square, Codex.Entities.occultist); if (Square.Character != null) Square.Character.SetNeutral(true); }
                                else
                                {
                                    if (SPDRandom.Int(2) == 0) Generator.PlaceCharacter(Square, 10, 40, E => E.Kind == Codex.Kinds.mummy);
                                    else Generator.PlaceCharacter(Square, 10, 40, E => E.Kind == Codex.Kinds.zombie);
                                }
                                break;
                            case 16: //forest neutral
                                switch (SPDRandom.Int(6))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.treant); break;
                                    case 3: case 4: Generator.PlaceCharacter(Square, 1, 32, E => E.Kind == Codex.Kinds.elf); break;
                                    case 1: case 2: Generator.PlaceCharacter(Square, 10, 20, E => E.Kind == Codex.Kinds.horse); break;
                                }
                                if (Square.Character != null) Square.Character.SetNeutral(true); break;
                            case 18:
                                if (SPDRandom.Int(5) == 0) Generator.PlaceCharacter(Square, Codex.Entities.doppelganger);
                                else Generator.PlaceCharacter(Square, 6, 40, E => E.Kind == Codex.Kinds.mimic); break;
                            case 19:
                                switch (SPDRandom.Int(6))
                                {
                                    case 0: Generator.PlaceCharacter(Square, E => E.Kind == Codex.Kinds.flayer); break;
                                    case 1: Generator.PlaceCharacter(Square, E => E.Kind == Codex.Kinds.flayer); break;
                                    case 2: Generator.PlaceCharacter(Square, 15, 40, E => E.Kind == Codex.Kinds.angel); break;
                                    case 3: Generator.PlaceCharacter(Square, 20, 40, E => E.Kind == Codex.Kinds.demon); break;
                                    case 4: Generator.PlaceCharacter(Square, E => E.Kind == Codex.Kinds.devourer); break;
                                    case 5: Generator.PlaceCharacter(Square, E => E.Kind == Codex.Kinds.devourer); break;
                                }
                                Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor); break;
                            case 20:
                                switch (SPDRandom.Int(4))
                                {
                                    case 0: Generator.PlaceCharacter(Square, Codex.Entities.master_mind_flayer); break;
                                    case 1: Generator.PlaceCharacter(Square, Codex.Entities.disintegrator); break;
                                    case 2: Generator.PlaceCharacter(Square, Codex.Entities.genetic_engineer); break;
                                    case 3: Generator.PlaceCharacter(Square, Codex.Entities.dracolich); break;
                                }

                                Square.Floor.SetGround(SPDDebug.codex.Grounds.obsidian_floor);
                                if (Square.Character != null) BossCharacter = Square.Character; break;
                        }
                        if (Square.Character != null && Square.Character.Entity != Codex.Entities.giant_cockroach) Generator.PromoteCharacter(Square.Character, 28 + SPDRandom.Int(5) - Square.Character.Level);
                        break;
                }
            }
            Assert.Enable();
        }
        private void CreateSokobanBranch()
        {
            var SokobanSite = SPDDebug.generator.Adventure.World.AddSite("Sokoban");
            SPDDebug.currentBranchSite = SokobanSite;
            var Specials = Resources.Specials;

            var SokobanLevelArray = new[]
            {
                new { Number = 1, OptionArray = new [] { Specials.Sokoban1a, Specials.Sokoban1b, Specials.Sokoban1c, Specials.Sokoban1d, Specials.Sokoban1e  } },
                new { Number = 2, OptionArray = new [] { Specials.Sokoban2a, Specials.Sokoban2b, Specials.Sokoban2c, Specials.Sokoban2d, Specials.Sokoban2e, Specials.Sokoban2f, Specials.Sokoban2g  } },
                new { Number = 3, OptionArray = new [] { Specials.Sokoban3a, Specials.Sokoban3b, Specials.Sokoban3c, Specials.Sokoban3d, Specials.Sokoban3e, Specials.Sokoban3f, Specials.Sokoban3g  } },
                new { Number = 4, OptionArray = new [] { Specials.Sokoban4a, Specials.Sokoban4b, Specials.Sokoban4c, Specials.Sokoban4d } },
            };

            var SokobanBarrier = SPDDebug.codex.Barriers.jade_wall;
            var SokobanGround = SPDDebug.codex.Grounds.marble_floor;
            var SokobanGate = SPDDebug.codex.Gates.wooden_door;
            var SokobanBlock = SPDDebug.codex.Blocks.gold_boulder;
            var SokobanUpPortal = SPDDebug.codex.Portals.jade_ladder_up;
            var SokobanDownPortal = SPDDebug.codex.Portals.jade_ladder_down;

            Square PreviousLevelSquare = null;

            foreach (var Level in SokobanLevelArray)
            {
                var Map = Level.OptionArray.GetRandom();

                var Grid = SPDDebug.generator.LoadSpecialGrid(Map);

                var LevelMapName = "Sokoban " + Level.Number;

                var SokobanMap = SPDDebug.generator.Adventure.World.AddMap(LevelMapName, Grid.Width, Grid.Height);
                SokobanMap.SetDifficulty(SPDDebug.currentmap.depth);
                SetDifficultyGroup();
                SokobanMap.SetAtmosphere(SPDDebug.codex.Atmospheres.dungeon);

                if (Level == SokobanLevelArray.First())
                    SokobanMap.SetTerminal(true);

                var SokobanLevel = SokobanSite.AddLevel(SokobanLevelArray.Length - Level.Number + 1, SokobanMap);

                Square UpLevelSquare = null;

                for (var Column = 0; Column < Grid.Width; Column++)
                {
                    for (var Row = 0; Row < Grid.Height; Row++)
                    {
                        var Symbol = Grid[Column, Row];

                        var SokobanSquare = SokobanMap[Column, Row];

                        switch (Symbol)
                        {
                            case '.':
                                // floor - treasure/monster by chance
                                SPDDebug.generator.PlaceFloor(SokobanSquare, SokobanGround);
                                SokobanSquare.SetLit(true);

                                switch (Level.Number)
                                {
                                    case 1: // Sokoban 1 has 25 objects.
                                        if (SPDRandom.Int(3) == 0)
                                        {
                                            if (SPDRandom.Int(3) == 0) CreateTreasure(SokobanSquare);
                                            else CreateMonster(SokobanSquare);
                                        }
                                        break;
                                    case 2: // Sokoban 2 - 40 objects.
                                        if (SPDRandom.Int(2) == 0)
                                        {
                                            if (SPDRandom.Int(3) == 0) CreateTreasure(SokobanSquare);
                                            else CreateMonster(SokobanSquare);
                                        }
                                        break;
                                    case 3: // Sokoban 3 - 40 objects.
                                        if (SPDRandom.Int(5) > 2)
                                        {
                                            if (SPDRandom.Int(3) == 0) CreateTreasure(SokobanSquare);
                                            else CreateMonster(SokobanSquare);
                                        }
                                        break;
                                    case 4: // Sokoban 4 - 50 objects.
                                        if (SPDRandom.Int(2) == 0)
                                        {
                                            if (SPDRandom.Int(3) == 0) CreateTreasure(SokobanSquare);
                                            else CreateMonster(SokobanSquare);
                                        }
                                        break;
                                }
                                break;

                            case ' ':
                                // void.
                                break;

                            case '-':
                            case '|':
                                // walls.
                                SPDDebug.generator.PlacePermanentWall(SokobanSquare, SokobanBarrier, WallSegment.Cross);
                                SokobanSquare.SetLit(true);
                                break;

                            case '+':
                                // door.
                                SPDDebug.generator.PlaceFloor(SokobanSquare, SokobanGround);
                                if (SokobanGate == null)
                                {
                                    SPDDebug.generator.PlaceIllusionaryWall(SokobanSquare, SokobanBarrier, WallSegment.Cross);
                                }
                                else
                                {
                                    SPDDebug.generator.PlaceClosedHorizontalDoor(SokobanSquare, SokobanGate, SokobanBarrier);
                                    var Door = SokobanSquare.Door;
                                    if (Door != null)
                                    {
                                        Door.SetState(DoorState.Locked);
                                        Door.SetTrap(SPDDebug.generator.NewTrap(SPDDebug.codex.Devices.fire_trap));
                                    }
                                }
                                SokobanSquare.SetLit(true);
                                break;

                            case '<':
                                // up ladder.
                                SPDDebug.generator.PlaceFloor(SokobanSquare, SokobanGround);

                                UpLevelSquare = SokobanSquare;

                                SokobanLevel.SetTransitions(SokobanSquare, SokobanLevel.DownSquare);

                                SokobanSquare.SetLit(true);
                                break;

                            case '>':
                                // down ladder.
                                SPDDebug.generator.PlaceFloor(SokobanSquare, SokobanGround);

                                var DestinationSquare = PreviousLevelSquare ?? null;

                                if (DestinationSquare != null) SPDDebug.generator.PlacePassage(DestinationSquare, SokobanUpPortal, SokobanSquare);
                                SPDDebug.generator.PlacePassage(SokobanSquare, SokobanDownPortal, DestinationSquare);

                                SokobanLevel.SetTransitions(SokobanLevel.UpSquare, SokobanSquare);

                                SokobanSquare.SetLit(true);

                                SokobanSquare.InsertTrigger().AddSchedule(Delay.Zero, SPDDebug.codex.Tricks.complete_mapping);
                                break;

                            case '0':
                                SPDDebug.generator.PlaceFloor(SokobanSquare, SokobanGround);
                                SPDDebug.generator.PlaceBoulder(SokobanSquare, SokobanBlock, true);

                                if (SPDRandom.Int(3) == 0) CreateTreasure(SokobanSquare);

                                SokobanSquare.SetLit(true);
                                break;

                            case '^':
                                SPDDebug.generator.PlaceFloor(SokobanSquare, SokobanGround);
                                if (PreviousLevelSquare == null)
                                    SPDDebug.generator.PlaceTrap(SokobanSquare, SPDDebug.codex.Devices.pit);
                                else
                                    SPDDebug.generator.PlaceTrap(SokobanSquare, SPDDebug.codex.Devices.hole);

                                var Trap = SokobanSquare.Trap;
                                if (Trap != null)
                                    Trap.SetRevealed(true);

                                SokobanSquare.SetLit(true);
                                break;

                            case '#':
                                // iron bars.
                                SPDDebug.generator.PlacePermanentWall(SokobanSquare, SokobanBarrier, WallSegment.Cross);
                                SokobanSquare.SetLit(true);
                                break;

                            case '}':
                                // water.
                                SPDDebug.generator.PlaceFloor(SokobanSquare, SPDDebug.codex.Grounds.water);
                                SokobanSquare.SetLit(true);

                                if (SPDDebug.generator.CanPlaceAsset(SokobanSquare) && Chance.OneIn10.Hit())
                                    SPDDebug.generator.PlaceSpecificAsset(SokobanSquare, SPDDebug.codex.Items.kelp_frond);
                                break;

                            case '$':
                                SPDDebug.generator.PlaceFloor(SokobanSquare, SokobanGround);
                                SokobanSquare.SetLit(true);

                                SPDDebug.generator.PlaceSpecificAsset(SokobanSquare, SPDDebug.gamelist.BranchTreasure(DifficultyGroup));
                                break;

                            case '&':
                                SPDDebug.generator.PlaceFloor(SokobanSquare, SokobanGround);
                                SokobanSquare.SetLit(true);

                                CreateTreasure(SokobanSquare);
                                
                                SokobanLevel.SetTransitions(SokobanSquare, SokobanLevel.DownSquare);
                                break;

                            case 'z':
                                SPDDebug.generator.PlaceFloor(SokobanSquare, SokobanGround);
                                SokobanSquare.SetLit(true);
                                if (SPDRandom.Int(2) == 0) CreateTreasure(SokobanSquare);
                                else CreateMonster(SokobanSquare);
                                break;

                            default:
                                SPDDebug.generator.PlaceFloor(SokobanSquare, SokobanGround);
                                break;
                        }
                    }
                }

                void CreateTreasure(Square TreasureSquare)
                {
                    switch (SPDRandom.Int(5))
                    {
                        case 0:
                            {
                                if (SPDRandom.Int(2) == 0) SPDDebug.generator.PlaceSpecificAsset(TreasureSquare, SPDDebug.codex.Items.chest);
                                else SPDDebug.generator.PlaceSpecificAsset(TreasureSquare, SPDDebug.codex.Items.large_box);
                                foreach (var Asset in TreasureSquare.GetAssets())
                                {
                                    if (Asset.Container != null)
                                    {
                                        Asset.Container.Stash.Add(SPDDebug.generator.NewSpecificAsset(TreasureSquare, SPDDebug.gamelist.BranchTreasure(DifficultyGroup)));
                                        if (SPDRandom.Int(4) == 0) SetContainer(TreasureSquare, Asset.Item.Storage, Asset.Container, Locked: true, Trapped: SPDRandom.Int(3) == 0);
                                    }
                                    else
                                    {
                                        TreasureSquare.RemoveAsset(Asset);
                                        SPDDebug.generator.PlaceRandomAsset(TreasureSquare, Asset.Item.Stock);
                                    }
                                }
                                break;
                            }
                        case 1: case 2:
                            {
                                if (SPDRandom.Int(2) == 0) SPDDebug.generator.PlaceSpecificAsset(TreasureSquare, SPDDebug.codex.Items.chest);
                                else SPDDebug.generator.PlaceSpecificAsset(TreasureSquare, SPDDebug.codex.Items.large_box);
                                foreach (var Asset in TreasureSquare.GetAssets())
                                {
                                    if (Asset.Container != null)
                                    {
                                        Asset.Container.Stash.Add(SPDDebug.gamelist.RandomAsset(TreasureSquare));
                                        if (SPDRandom.Int(4) == 0)
                                            SetContainer(TreasureSquare, Asset.Item.Storage, Asset.Container, Locked: true, Trapped: SPDRandom.Int(3) == 0);
                                    }
                                    else
                                    {
                                        TreasureSquare.RemoveAsset(Asset);
                                        SPDDebug.generator.PlaceRandomAsset(TreasureSquare, Asset.Item.Stock);
                                    }
                                }
                                break;
                            }
                        case 3: case 4: SPDDebug.generator.PlaceSpecificAsset(TreasureSquare, SPDDebug.codex.Items.gold_coin, RandomGoldCoin()); break;
                    }
                }

                int RandomGoldCoin()
                {
                    switch (DifficultyGroup)
                    {
                        case 1: return 50;
                        case 2: return 100;
                        case 3: return 150;
                        case 4: return 200;
                        case 5: return 250;
                        case 6: return 300;
                        case 7: return 350;
                        case 8: return 400;
                    }
                    return 100;
                }

                void CreateMonster(Square MonsterSquare)
                {
                    // higher chance for mimics, lower chance for hiding entities
                    switch (DifficultyGroup)
                    {
                        case 1: switch (SPDRandom.Int(6))
                            {
                                case 0: case 1: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.small_mimic); break;
                                case 2: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.small_mimic); break;
                                case 3: case 4: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.large_mimic); break;
                                case 5: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.pile_of_killer_coins); break;
                            }
                            break;
                        case 2:
                            switch (SPDRandom.Int(8))
                            {
                                case 0: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.rock_piercer); break;
                                case 1: case 5: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.small_mimic); break;
                                case 2: case 6: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.large_mimic); break;
                                case 3: case 7: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.pile_of_killer_coins); break;
                                case 4: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.stalker); break;
                            }
                            if (MonsterSquare.Character != null) SPDDebug.generator.PromoteCharacter(MonsterSquare.Character, 6 + SPDRandom.Int(4) - MonsterSquare.Character.Level);
                            break;
                        case 3:
                            switch (SPDRandom.Int(8))
                            {
                                case 0: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.glass_piercer); break;
                                case 1: case 5: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.small_mimic); break;
                                case 2: case 6: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.large_mimic); break;
                                case 3: case 7: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.pile_of_killer_coins); break;
                                case 4: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.stalker); break;
                            }
                            if (MonsterSquare.Character != null) SPDDebug.generator.PromoteCharacter(MonsterSquare.Character, 10 + SPDRandom.Int(4) - MonsterSquare.Character.Level);
                            break;
                        case 4:
                            switch (SPDRandom.Int(6))
                            {
                                case 0: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.glass_piercer); break;
                                case 1: case 4: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.small_mimic); break;
                                case 2: case 5: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.large_mimic); break;
                                case 3: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.large_pile_of_killer_coins); break;
                            }
                            if (MonsterSquare.Character != null) SPDDebug.generator.PromoteCharacter(MonsterSquare.Character, 14 + SPDRandom.Int(4) - MonsterSquare.Character.Level);
                            break;
                        case 5:
                            switch (SPDRandom.Int(6))
                            {
                                case 0: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.iron_piercer); break;
                                case 1: case 4: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.small_mimic); break;
                                case 2: case 5: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.large_mimic); break;
                                case 3: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.large_pile_of_killer_coins); break;
                            }
                            if (MonsterSquare.Character != null) SPDDebug.generator.PromoteCharacter(MonsterSquare.Character, 18 + SPDRandom.Int(4) - MonsterSquare.Character.Level);
                            break;
                        case 6:
                            switch (SPDRandom.Int(4))
                            {
                                case 0: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.lurker_above); break;
                                case 1: case 4: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.small_mimic); break;
                                case 2: case 5: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.large_mimic); break;
                                case 3: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.large_pile_of_killer_coins); break;
                            }
                            if (MonsterSquare.Character != null) SPDDebug.generator.PromoteCharacter(MonsterSquare.Character, 22 + SPDRandom.Int(5) - MonsterSquare.Character.Level);
                            break;
                        case 7:
                            switch (SPDRandom.Int(4))
                            {
                                case 0: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.lurker_above); break;
                                case 1: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.small_mimic); break;
                                case 2: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.large_mimic); break;
                                case 3: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.huge_pile_of_killer_coins); break;
                            }
                            if (MonsterSquare.Character != null) SPDDebug.generator.PromoteCharacter(MonsterSquare.Character, 25 + SPDRandom.Int(7) - MonsterSquare.Character.Level);
                            break;
                        case 8:
                            switch (SPDRandom.Int(5))
                            {
                                case 0: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.trapper); break;
                                case 1: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.small_mimic); break;
                                case 2: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.large_mimic); break;
                                case 3: SPDDebug.generator.PlaceCharacter(MonsterSquare, SPDDebug.codex.Entities.huge_pile_of_killer_coins); break;
                            }
                            if (MonsterSquare.Character != null) SPDDebug.generator.PromoteCharacter(MonsterSquare.Character, 30 + SPDRandom.Int(5) - MonsterSquare.Character.Level);
                            break;
                    }
                }

                SPDDebug.generator.RepairVoid(SokobanMap, SokobanMap.Region);
                SPDDebug.generator.RepairWalls(SokobanMap, SokobanMap.Region);
                SPDDebug.generator.RepairDoors(SokobanMap, SokobanMap.Region);
                SPDDebug.generator.RepairZones(SokobanMap, SokobanMap.Region);

                foreach (var Zone in SokobanMap.Zones)
                    Zone.SetRestricted(true);

                PreviousLevelSquare = UpLevelSquare;
            }
        }
        private void CreateLabyrinthBranch()
        {
            var LabyrinthBarrier = SPDDebug.codex.Barriers.hell_brick;
            var LabyrinthBlock = SPDDebug.codex.Blocks.crystal_boulder;
            var LabyrinthGround = SPDDebug.codex.Grounds.obsidian_floor;
            var LabyrinthLevelDownPortal = SPDDebug.codex.Portals.stone_staircase_down;
            var LabyrinthLevelUpPortal = SPDDebug.codex.Portals.stone_staircase_up;

            // three maze levels, 10x10 -> 20x20 -> 30x30.
            var LabyrinthSite = SPDDebug.generator.Adventure.World.AddSite("Labyrinth");
            SPDDebug.currentBranchSite = LabyrinthSite;

            var BaseSize = 11;
            var BoxSize = 5;
            var LevelCount = 3;

            for (var LevelIndex = 1; LevelIndex <= LevelCount; LevelIndex++)
            {
                var LabyrinthSize = BaseSize * LevelIndex;
                var LabyrinthMap = SPDDebug.generator.Adventure.World.AddMap("Labyrinth " + LevelIndex, LabyrinthSize, LabyrinthSize);
                LabyrinthMap.SetDifficulty(SPDDebug.currentmap.depth);
                SetDifficultyGroup();
                LabyrinthMap.SetAtmosphere(SPDDebug.codex.Atmospheres.nether);
                LabyrinthMap.SetTerminal(LevelIndex == LevelCount);

                var LabyrinthLevel = LabyrinthSite.AddLevel(LevelIndex, LabyrinthMap);
                var LabyrinthRegion = LabyrinthMap.Region;

                LabyrinthMap.GetSquares(LabyrinthRegion).ForEach(S => SPDDebug.generator.PlaceSolidWall(S, LabyrinthBarrier, WallSegment.Cross));
                CreateMazePaths(LabyrinthMap, LabyrinthRegion);
                CreateMazeDetails(LabyrinthMap, LabyrinthBlock, LabyrinthRegion, BoxSize);

                var ExclusionSize = BoxSize * LevelIndex;
                var ExclusionMargin = (LabyrinthSize - ExclusionSize) / 2;
                var ExclusionRegion = new Region(ExclusionMargin, ExclusionMargin, LabyrinthSize - ExclusionMargin, LabyrinthSize - ExclusionMargin);

                // upstairs.
                var LevelUpSquare = LabyrinthMap.GetSquares().Where(S => SPDDebug.generator.CanPlacePortal(S) && !S.IsRegion(ExclusionRegion)).GetRandomOrNull();
                if (LevelUpSquare == null)
                    LevelUpSquare = LabyrinthMap.GetSquares().Where(SPDDebug.generator.CanPlacePortal).ToArray().GetRandom();
                SPDDebug.generator.PlacePassage(LevelUpSquare, LabyrinthLevelUpPortal, Destination: null);

                // downstairs.
                var LevelDownSquare = LabyrinthMap.GetSquares().Where(S => SPDDebug.generator.CanPlacePortal(S) && S.AsRange(LevelUpSquare) >= ExclusionSize).GetRandomOrNull();
                if (LevelDownSquare == null)
                    LevelDownSquare = LabyrinthMap.GetSquares().Where(SPDDebug.generator.CanPlacePortal).ToArray().GetRandom();
                SPDDebug.generator.PlacePassage(LevelDownSquare, LabyrinthLevelDownPortal, Destination: null);

                LabyrinthLevel.SetTransitions(LevelUpSquare, LevelDownSquare);

                if (LabyrinthMap.Terminal)
                {
                    // replace some walls with lava.
                    foreach (var InnerSquare in LabyrinthMap.GetSquares(LabyrinthRegion.Contract()))
                    {
                        if (InnerSquare.Wall != null && Chance.OneIn10.Hit())
                        {
                            InnerSquare.SetWall(null);
                            SPDDebug.generator.PlaceFloor(InnerSquare, SPDDebug.codex.Grounds.lava);
                        }
                    }
                }

                SPDDebug.generator.RepairVoid(LabyrinthMap, LabyrinthRegion);
                SPDDebug.generator.RepairWalls(LabyrinthMap, LabyrinthRegion);
                SPDDebug.generator.RepairZones(LabyrinthMap, LabyrinthRegion);
            }

            Square CurrentSquare = null;

            foreach (var LabyrinthLevel in LabyrinthSite.Levels)
            {
                if (CurrentSquare != null) CurrentSquare.Passage.SetDestination(LabyrinthLevel.UpSquare);
                LabyrinthLevel.UpSquare.Passage.SetDestination(CurrentSquare);
                CurrentSquare = LabyrinthLevel.DownSquare;
            }

            // minotaur boss, obviously.
            SPDDebug.generator.PlaceCharacter(CurrentSquare, SPDDebug.codex.Entities.minotaur);

            var BossCharacter = CurrentSquare.Character;

            if (BossCharacter != null)
            {
                //SPDDebug.generator.PromoteCharacter(BossCharacter, EntranceMap.Difficulty - BossCharacter.Level + 5);

                var Properties = SPDDebug.codex.Properties;
                var Elements = SPDDebug.codex.Elements;

                BossCharacter.AcquireTalent(Properties.polymorph_control, Properties.slippery, Properties.free_action, Properties.clarity);
                BossCharacter.SetResistance(Elements.fire, 100);
                BossCharacter.SetResistance(Elements.magical, 100);
                BossCharacter.SetResistance(Elements.poison, 100);

                // master competency in all skills.
                foreach (var Competency in BossCharacter.Competencies)
                    Competency.Set(SPDDebug.codex.Qualifications.master);

                var ArtifactAsset = SPDDebug.generator.NewSpecificAsset(BossCharacter.Square, SPDDebug.codex.Items.wand_of_digging);
                BossCharacter.Inventory.Carried.Add(ArtifactAsset);
            }

            // artifact is placed under a boulder, as a homage to the original Hack's location of the Amulet of Yendor.
            Square GetJammedSquare() => CurrentSquare.Map.GetSquares().Where(S => SPDDebug.generator.CanPlaceBoulder(S) && S.IsJammed()).GetRandomOrNull();

            var JammedSquare = GetJammedSquare();
            if (JammedSquare != null)
            {
                var ArtifactItem = SPDDebug.generator.GetArtifactItem();
                if (ArtifactItem != null)
                    JammedSquare.PlaceAsset(SPDDebug.generator.NewSpecificAsset(JammedSquare, ArtifactItem));

                SPDDebug.generator.PlaceBoulder(JammedSquare, LabyrinthBlock, IsRigid: true); // rigid so monsters don't move it around.

                // 5-8 decoy boulders.
                var DecoyDice = 1.d4() + 4;
                for (var DecoyIndex = 0; DecoyIndex < DecoyDice.Roll(); DecoyIndex++)
                {
                    var DecoySquare = GetJammedSquare();
                    if (DecoySquare != null)
                        SPDDebug.generator.PlaceBoulder(DecoySquare, LabyrinthBlock, IsRigid: true); // rigid so monsters don't move it around.
                }
            }
            else if (BossCharacter != null)
            {
                // carrying the artifact instead.
                SPDDebug.generator.AcquireArtifact(CurrentSquare, BossCharacter, SPDDebug.codex.Qualifications.master);
            }
        }
        private void CreateFortBranch()
        {
            var FortItems = SPDDebug.codex.Items;
            var FortEntities = SPDDebug.codex.Entities;

            var FortSoldierArray = FortEntities.List.Where(E => E.Startup.HasSkill(SPDDebug.codex.Skills.firearms) && !E.IsMercenary && E.IsEncounter).OrderBy(E => E.Level).ToArray();

            var FortFirearmArray = FortItems.List.Where(I => I.Weapon != null && I.Weapon.Skill == SPDDebug.codex.Skills.firearms && !I.Artifact && I.Price > Gold.One).ToArray();

            var FortSite = SPDDebug.generator.Adventure.World.AddSite("Fort");
            SPDDebug.currentBranchSite = FortSite;

            var FortArray = new[]
            {
                Resources.Specials.FortLudios1,
                Resources.Specials.FortLudios2,
                Resources.Specials.FortLudios3,
                Resources.Specials.FortLudios4,
                Resources.Specials.FortLudios5
            };

            var FortGrid = SPDDebug.generator.LoadSpecialGrid(FortArray.GetRandom());
            var FortWidth = FortGrid.Width;
            var FortHeight = FortGrid.Height;
            var FortMap = SPDDebug.generator.Adventure.World.AddMap("Fort", FortWidth, FortHeight);
            FortMap.SetDifficulty(SPDDebug.currentmap.depth);
            SetDifficultyGroup();
            FortMap.SetAtmosphere(SPDDebug.codex.Atmospheres.civilisation);

            var FortLevel = FortSite.AddLevel(1, FortMap);

            var FortGate = SPDDebug.codex.Gates.wooden_door;
            var FortBarrier = SPDDebug.codex.Barriers.stone_wall;
            var FortRoomGround = SPDDebug.codex.Grounds.stone_floor;
            var FortPathGround = SPDDebug.codex.Grounds.stone_corridor;
            var FortWaterGround = SPDDebug.codex.Grounds.water;

            var FortSoldierMaxChallenge = FortSoldierArray.Max(E => E.Challenge);
            var FortFirearmMaxEssence = FortFirearmArray.Max(I => I.Essence);

            // NOTE: deliberately zero probability for the highest level soldier to be generated.
            var FortSoldierProbability = FortSoldierArray.ToProbability(E => FortSoldierMaxChallenge - E.Challenge);
            var FortGemProbability = FortItems.List.Where(I => I.Type == ItemType.Gem && I.Price > Gold.One && !I.Artifact).ToProbability(I => I.Rarity);
            var FortFirearmProbability = FortFirearmArray.ToProbability(I => (FortFirearmMaxEssence - I.Essence).GetUnits() + 1);
            var FortDogProbability = new[] { FortEntities.pit_bull, FortEntities.large_dog, FortEntities.dog, FortEntities.death_dog, FortEntities.hell_hound }.ToProbability(I => I.Frequency);
            var FortFoodProbability = new[] { FortItems.cration, FortItems.kration }.ToProbability(I => I.Rarity);

            for (var Row = 0; Row < FortHeight; Row++)
            {
                for (var Column = 0; Column < FortWidth; Column++)
                {
                    var FortSquare = FortMap[Column, Row];

                    var FortSymbol = FortGrid[Column, Row];
                    
                    switch (FortSymbol)
                    {
                        case ' ':
                            // void.
                            break;

                        case '|':
                        case '-':
                            // walls.
                            SPDDebug.generator.PlacePermanentWall(FortSquare, FortBarrier, WallSegment.Cross);
                            FortSquare.SetLit(true);
                            break;

                        case 'x':
                            // inside floor.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            FortSquare.SetLit(true);
                            break;

                        case '.':
                            // outside floor.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortPathGround);
                            FortSquare.SetLit(false);
                            break;

                        case '}':
                            // water.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortWaterGround);
                            FortSquare.SetLit(false);

                            if (SPDDebug.generator.CanPlaceAsset(FortSquare) && Chance.OneIn10.Hit())
                                SPDDebug.generator.PlaceSpecificAsset(FortSquare, SPDDebug.codex.Items.kelp_frond);
                            break;

                        case ';':
                            // water monster.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortWaterGround);
                            SPDDebug.generator.PlaceCharacter(FortSquare);
                            FortSquare.SetLit(false);
                            break;

                        case '*':
                            // precious gems.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            SPDDebug.generator.PlaceSpecificAsset(FortSquare, FortGemProbability.GetRandomOrNull());
                            FortSquare.SetLit(true);
                            break;

                        case ')':
                            // firearms.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            //SPDDebug.generator.PlaceSpecificAsset(FortSquare, SPDDebug.generator.FirearmReplacement(FortFirearmProbability.GetRandomOrNull()));
                            FortSquare.SetLit(true);
                            break;

                        case '%':
                            // rations.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            SPDDebug.generator.PlaceSpecificAsset(FortSquare, FortFoodProbability.GetRandomOrNull());
                            FortSquare.SetLit(true);
                            break;

                        case '(':
                            // chests.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);

                            FortSquare.SetLit(true);
                            break;

                        case '^':
                            // start portal.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            FortLevel.SetTransitions(FortSquare, FortSquare);
                            FortSquare.SetLit(true);
                            break;

                        case '+':
                            // door.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            SPDDebug.generator.PlaceLockedHorizontalDoor(FortSquare, FortGate, FortBarrier);
                            FortSquare.SetLit(true);
                            break;

                        case 'S':
                            // secret door.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            SPDDebug.generator.PlaceSecretHorizontalDoor(FortSquare, FortGate, FortBarrier);
                            FortSquare.SetLit(true);
                            break;

                        case '$':
                            // coins and traps.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            FortSquare.SetLit(true);

                            if (Chance.OneIn3.Hit())
                            {
                                if (Chance.OneIn3.Hit())
                                    SPDDebug.generator.PlaceTrap(FortSquare, SPDDebug.codex.Devices.spiked_pit);
                                else
                                    SPDDebug.generator.PlaceTrap(FortSquare, SPDDebug.codex.Devices.explosive_trap);
                            }
                            break;

                        case '@':
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            SPDDebug.generator.PlaceCharacter(FortSquare, FortSoldierProbability.GetRandomOrNull());

                            var BarracksCharacter = FortSquare.Character;
                            if (BarracksCharacter != null)
                                SnoozeCharacter(BarracksCharacter);

                            if (Chance.OneIn8.Hit())
                                SPDDebug.generator.PlaceFixture(FortSquare, SPDDebug.codex.Features.bed);

                            FortSquare.SetLit(true);
                            break;

                        case 'G':
                            // outside soldiers.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortPathGround);
                            SPDDebug.generator.PlaceCharacter(FortSquare, FortSoldierArray[0]); // lowest level.
                            FortSquare.SetLit(false);
                            break;

                        case 'D':
                            // outside dragons.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortPathGround);
                            SPDDebug.generator.PlaceCharacter(FortSquare, E => E.Kind == SPDDebug.codex.Kinds.dragon);
                            FortSquare.SetLit(false);
                            break;

                        case 'd':
                            // inside dogs.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            SPDDebug.generator.PlaceCharacter(FortSquare, FortDogProbability.GetRandomOrNull());
                            FortSquare.SetLit(true);
                            break;

                        case 'o':
                            // inside orcs.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            SPDDebug.generator.PlaceCharacter(FortSquare, E => E.Race == SPDDebug.codex.Races.orc);
                            FortSquare.SetLit(true);
                            break;

                        case 'B':
                            // inside boss.
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            SPDDebug.generator.PlaceCharacter(FortSquare, FortSoldierArray[FortSoldierArray.Length - 1]);
                            SPDDebug.generator.PlaceFixture(FortSquare, SPDDebug.codex.Features.throne);
                            FortSquare.SetLit(true);

                            var FortCharacter = FortSquare.Character;
                            if (FortCharacter != null)
                            {
                                SPDDebug.generator.PromoteCharacter(FortCharacter, FortMap.Difficulty - FortCharacter.Level + 5);

                                var Properties = SPDDebug.codex.Properties;
                                var Elements = SPDDebug.codex.Elements;

                                FortCharacter.AcquireTalent(Properties.clarity, Properties.free_action, Properties.slippery, Properties.polymorph_control, Properties.see_invisible, Properties.vitality);
                                FortCharacter.SetResistance(Elements.drain, 100);
                                FortCharacter.SetResistance(Elements.cold, 100);
                                FortCharacter.SetResistance(Elements.fire, 100);
                                FortCharacter.SetResistance(Elements.sleep, 100);
                                FortCharacter.SetResistance(Elements.magical, 100);
                            }

                            FortLevel.SetTransitions(FortLevel.UpSquare, FortSquare);
                            break;

                        case 't':
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            SPDDebug.generator.PlaceCharacter(FortSquare); // this is a throne room.

                            var ThroneCharacter = FortSquare.Character;
                            if (ThroneCharacter != null)
                                SnoozeCharacter(ThroneCharacter);

                            FortSquare.SetLit(true);
                            break;

                        case 'z':
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            SPDDebug.generator.PlaceCharacter(FortSquare); //TODO: Drop coins

                            var ZooCharacter = FortSquare.Character;
                            if (ZooCharacter != null)
                                SnoozeCharacter(ZooCharacter);

                            FortSquare.SetLit(true);
                            break;

                        default:
                            SPDDebug.generator.PlaceFloor(FortSquare, FortRoomGround);
                            break;
                    }
                }
            }

            SPDDebug.generator.RepairVoid(FortMap, FortMap.Region);
            SPDDebug.generator.RepairWalls(FortMap, FortMap.Region);
            SPDDebug.generator.RepairDoors(FortMap, FortMap.Region);
            SPDDebug.generator.RepairZones(FortMap, FortMap.Region);

            foreach (var Zone in FortMap.Zones)
                Zone.SetRestricted(true);

            var CacheGate = SPDDebug.codex.Gates.wooden_door;

            var CacheWidth = 12;
            var CacheHeight = 12;
            var CacheMap = SPDDebug.generator.Adventure.World.AddMap("Fort Cache", CacheWidth, CacheHeight);
            CacheMap.SetDifficulty(SPDDebug.currentmap.depth);
            CacheMap.SetTerminal(true);
            CacheMap.SetAtmosphere(SPDDebug.codex.Atmospheres.civilisation);

            var CacheLevel = FortSite.AddLevel(2, CacheMap);
            var CacheGrid = SPDDebug.generator.LoadSpecialGrid(Resources.Specials.FortLudiosCache);

            for (var Row = 0; Row < CacheHeight; Row++)
            {
                for (var Column = 0; Column < CacheWidth; Column++)
                {
                    var CacheSquare = CacheMap[Column, Row];
                    var CacheSymbol = CacheGrid[Column, Row];

                    switch (CacheSymbol)
                    {
                        case '|':
                        case '-':
                            // walls.
                            SPDDebug.generator.PlacePermanentWall(CacheSquare, FortBarrier, WallSegment.Cross);
                            CacheSquare.SetLit(false);
                            break;

                        case '.':
                            // room.
                            SPDDebug.generator.PlaceFloor(CacheSquare, FortRoomGround);
                            CacheSquare.SetLit(false);
                            break;

                        case '#':
                            // corridor.
                            SPDDebug.generator.PlaceFloor(CacheSquare, FortPathGround);
                            CacheSquare.SetLit(false);
                            break;

                        case '@':
                            SPDDebug.generator.PlaceFloor(CacheSquare, FortRoomGround);
                            SPDDebug.generator.PlaceCharacter(CacheSquare, FortSoldierProbability.GetRandomOrNull());

                            var BarracksCharacter = CacheSquare.Character;
                            if (BarracksCharacter != null)
                                SnoozeCharacter(BarracksCharacter);

                            CacheSquare.SetLit(false);
                            break;

                        case '+':
                            // door.
                            SPDDebug.generator.PlaceFloor(CacheSquare, FortRoomGround);
                            SPDDebug.generator.PlaceClosedHorizontalDoor(CacheSquare, FortGate, FortBarrier);
                            CacheSquare.SetLit(false);
                            break;

                        case 'S':
                            // secret door.
                            SPDDebug.generator.PlaceFloor(CacheSquare, FortRoomGround);
                            SPDDebug.generator.PlaceSecretHorizontalDoor(CacheSquare, CacheGate, FortBarrier);
                            CacheSquare.SetLit(false);
                            break;

                        case '*':
                            // gems.
                            SPDDebug.generator.PlaceFloor(CacheSquare, FortRoomGround);
                            SPDDebug.generator.PlaceSpecificAsset(CacheSquare, FortGemProbability.GetRandomOrNull());
                            CacheSquare.SetLit(false);
                            break;

                        case '(':
                            // chests.
                            SPDDebug.generator.PlaceFloor(CacheSquare, FortRoomGround);
                            //TODO: PlaceContainer(CacheSquare, Locked: true, Trapped: true);
                            CacheSquare.SetLit(false);
                            break;

                        case 'z':
                            SPDDebug.generator.PlaceFloor(CacheSquare, FortRoomGround);
                            SPDDebug.generator.PlaceCharacter(CacheSquare); // this is a zoo.
                            //TODO: DropCoins(CacheSquare, SPDDebug.generator.RandomCoinQuantity(CacheSquare) * 4.d4().Roll());

                            var ZooCharacter = CacheSquare.Character;
                            if (ZooCharacter != null)
                                SnoozeCharacter(ZooCharacter);

                            CacheSquare.SetLit(false);
                            break;

                        case '^':
                            // exit portal.
                            SPDDebug.generator.PlaceFloor(CacheSquare, FortRoomGround);
                            CacheLevel.SetTransitions(null, CacheSquare);
                            CacheSquare.SetLit(true);

                            CacheLevel.SetTransitions(CacheSquare, CacheSquare);
                            break;

                        case 'D':
                            // surprise dragon.
                            SPDDebug.generator.PlaceFloor(CacheSquare, FortRoomGround);
                            SPDDebug.generator.PlaceCharacter(CacheSquare, E => E.Kind.Name == "dragon");
                            CacheSquare.SetLit(false);
                            break;

                        default:
                            SPDDebug.generator.PlaceFloor(CacheSquare, FortRoomGround);
                            break;
                    }
                }
            }

            SPDDebug.generator.RepairVoid(CacheMap, CacheMap.Region);
            SPDDebug.generator.RepairWalls(CacheMap, CacheMap.Region);
            SPDDebug.generator.RepairDoors(CacheMap, CacheMap.Region);

            // entry point.
            var CacheZone = CacheMap.AddZone();
            CacheZone.AddSquare(CacheMap[8, 4]);

            SPDDebug.generator.RepairZones(CacheMap, CacheMap.Region);

            foreach (var Zone in CacheMap.Zones.Except(CacheZone))
                Zone.SetRestricted(true);
        }
        private void CreateLairBranch()
        {
            var Quest = SPDDebug.generator.ImportQuest(Resources.Quests.Lair.GetBuffer());
            var QuestSite = Quest.World.Sites.Single();
            var QuestStart = Quest.World.Start;

            var LairSite = SPDDebug.generator.Adventure.World.AddSite("Lair");
            SPDDebug.currentBranchSite = LairSite;

            var LairDifficulty = SPDDebug.currentmap.depth;
            SetDifficultyGroup();

            var MercenaryProbability = SPDDebug.codex.Entities.List.Where(E => E.IsMercenary && E.IsEncounter).ToProbability(E => E.Frequency);

            var TreasureItemList = new Inv.DistinctList<Item>
            {
                SPDDebug.codex.Items.amulet_versus_stone,
                SPDDebug.codex.Items.shield_of_reflection,
                SPDDebug.codex.Items.blindfold,
                SPDDebug.codex.Items.helm_of_telepathy,
                SPDDebug.codex.Items.ring_of_free_action
            };

            Item GetTreasureItem()
            {
                var Result = TreasureItemList.GetRandomOrNull();

                if (Result != null)
                    TreasureItemList.Remove(Result);

                return Result;
            }

            foreach (var QuestLevel in QuestSite.Levels)
            {
                var LairMap = QuestLevel.Map;
                LairMap.SetName("Lair " + QuestLevel.Index);
                LairMap.SetDifficulty(LairDifficulty + QuestLevel.Index);
                LairMap.SetAtmosphere(SPDDebug.codex.Atmospheres.forest);
                LairMap.SetTerminal(QuestLevel == QuestSite.LastLevel);

                SPDDebug.generator.Adventure.World.AddMap(LairMap);

                var LairLevel = LairSite.AddLevel(QuestLevel.Index, LairMap);
                LairLevel.SetTransitions(QuestLevel.UpSquare, QuestLevel.DownSquare);

                SPDDebug.generator.BuildMap(LairMap);

                foreach (var LairSquare in LairMap.GetSquares())
                {

                    if (LairSquare.Zone != null)
                        LairSquare.Zone.SetRestricted(true);

                    if (LairSquare.Wall != null && LairSquare.Wall.IsPhysical())
                        LairSquare.Wall.SetStructure(WallStructure.Permanent);

                    if (LairSquare.Boulder != null && LairSquare.Boulder.Block.Prison)
                        LairSquare.Boulder.SetPrisoner(SPDDebug.generator.NewCharacter(LairSquare, MercenaryProbability.GetRandom()));

                    if (LairSquare.Passage != null && LairSquare.Passage.Destination == null)
                        LairLevel.SetTransitions(LairLevel.UpSquare, LairSquare);

                    foreach (var Asset in LairSquare.GetAssets())
                    {
                        if (Asset.Container != null)
                        {
                            // random container loot.
                            if (Asset.Item.Type == ItemType.Chest && Asset.Container.Stash.Count == 0)
                            {
                                //TODO: StockContainer(LairSquare, Asset.Item.Storage, Asset.Container, Locked: true, Trapped: true);

                                if (QuestLevel == QuestSite.LastLevel)
                                    Asset.Container.Stash.Add(SPDDebug.generator.NewSpecificAsset(LairSquare, GetTreasureItem()));
                            }
                        }
                        else
                        {
                            // randomise items.
                            LairSquare.RemoveAsset(Asset);
                            SPDDebug.generator.PlaceRandomAsset(LairSquare, Asset.Item.Stock);
                        }
                    }

                    var LairCharacter = LairSquare.Character;
                    if (LairCharacter != null && LairCharacter.Entity == SPDDebug.codex.Entities.medusa)
                    {
                        SPDDebug.generator.PromoteCharacter(LairCharacter, LairMap.Difficulty - LairCharacter.Level + 5);

                        var Properties = SPDDebug.codex.Properties;
                        var Elements = SPDDebug.codex.Elements;

                        LairCharacter.AcquireTalent(Properties.clarity, Properties.life_regeneration, Properties.mana_regeneration, Properties.polymorph_control, Properties.see_invisible, Properties.vitality, Properties.slippery, Properties.free_action);
                        LairCharacter.SetResistance(Elements.drain, 100);
                        LairCharacter.SetResistance(Elements.cold, 100);
                        LairCharacter.SetResistance(Elements.fire, 100);
                        LairCharacter.SetResistance(Elements.sleep, 100);
                        LairCharacter.SetResistance(Elements.magical, 100);

                        // maximum knowledge of the known spells.
                        var SpellArray = new[]
                        {
                            SPDDebug.codex.Spells.invisibility,
                            SPDDebug.codex.Spells.haste,
                            SPDDebug.codex.Spells.summoning,
                            SPDDebug.codex.Spells.poison_blast,
                            SPDDebug.codex.Spells.toxic_spray,
                            SPDDebug.codex.Spells.darkness,
                        };

                        foreach (var Spell in SpellArray)
                            LairCharacter.Knowledge.LearnSpell(Spell, 4);

                        // master competency in all skills.
                        foreach (var Competency in LairCharacter.Competencies)
                            Competency.Set(SPDDebug.codex.Qualifications.master);

                        var SchoolSkillArray = SpellArray.Select(S => S.School.Skill).Distinct().ToArray();
                        foreach (var SchoolSkill in SchoolSkillArray.Except(LairCharacter.Competencies.Select(C => C.Skill)))
                            LairCharacter.AddCompetency(SchoolSkill, SPDDebug.codex.Qualifications.master);

                        var ArtifactItem = SPDDebug.generator.GetArtifactItem();

                        if (ArtifactItem != null)
                        {
                            LairCharacter.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(LairSquare, ArtifactItem));

                            // medusa will not use the artifact.
                            //SPDDebug.generator.OutfitCharacter(LairCharacter);
                        }
                    }
                }
            }
            LairSite.FirstLevel.SetTransitions(QuestStart, LairSite.FirstLevel.DownSquare);
        }
        private void CreateKingdomBranch()
        {
            var StandardGeneration = Chance.ThreeIn4.Hit(); // 75% normal, 25% massacre.

            var ResourceFile = StandardGeneration ? Resources.Quests.Kingdom1 : Resources.Quests.Kingdom2;

            var Quest = SPDDebug.generator.ImportQuest(ResourceFile.GetBuffer());
            var QuestSite = Quest.World.Sites.Single();
            var QuestStart = Quest.World.Start;

            var KingdomSite = SPDDebug.generator.Adventure.World.AddSite("Kingdom");
            SPDDebug.currentBranchSite = KingdomSite;

            var KingdomDifficulty = SPDDebug.currentmap.depth;
            SetDifficultyGroup();
            Square InnerPortalSquare = null;
            var TownParty = SPDDebug.generator.NewParty(Leader: null);

            void GenerateMap(Map KingdomMap)
            {
                KingdomMap.RotateRandomDegrees();

                var IsTown = KingdomMap.Name == "Elf Town";
                var IsPalace1 = KingdomMap.Name.StartsWith("Elf Palace 1");

                KingdomMap.SetName(SPDDebug.generator.EscapeTranslatedName(KingdomMap.Name));

                if (IsPalace1)
                {
                    var PentagramZone = KingdomMap.AddZone();
                    PentagramZone.AddRegion(new Region(1, 6, 6, 11));

                    var Trigger = PentagramZone.InsertTrigger();

                    var SummonDelay = 1.d60();

                    for (var Repeat = 0; Repeat < 2.d3().Roll(); Repeat++)
                    {
                        foreach (var PentagramSquare in PentagramZone.Squares.Where(S => S.Fixture != null))
                            Trigger.AddSchedule(Delay.FromTurns(SummonDelay.Roll()), SPDDebug.codex.Tricks.summoning_demons).SetTarget(PentagramSquare);
                    }
                }

                SPDDebug.generator.BuildMap(KingdomMap);

                // randomised shops per level.
                var KingdomShopProbability = SPDDebug.codex.Shops.List.Where(S => !SPDDebug.adventure.Abolition || !S.SellsOnlyAbolitionCandidates()).ToProbability(M => M.Rarity).Clone();

                Shop NextRandomShop()
                {
                    if (!KingdomShopProbability.HasChecks())
                        KingdomShopProbability = SPDDebug.codex.Shops.List.Where(S => !SPDDebug.adventure.Abolition || !S.SellsOnlyAbolitionCandidates()).ToProbability(M => M.Rarity).Clone();

                    return KingdomShopProbability.RemoveRandomOrNull();
                }

                foreach (var KingdomSquare in KingdomMap.GetSquares())
                {
                    if (KingdomSquare.Passage != null && KingdomSquare.Passage.Destination == null && KingdomSquare != QuestStart && KingdomSquare.Passage.Portal == SPDDebug.codex.Portals.transportal)
                    {
                        if (KingdomMap.Level.Index == 1)
                            InnerPortalSquare = KingdomSquare;
                        else
                            KingdomSquare.SetPassage(null);
                    }

                    var Shrine = KingdomSquare.Character?.Resident?.Shrine;
                    if (Shrine != null)
                    {
                        // force a zone so we can have a visit trigger.
                        var ShrineZone = KingdomMap.AddZone();
                        ShrineZone.ForceRegion(KingdomSquare.FindBoundary());
                        ShrineZone.InsertTrigger().AddSchedule(Delay.Zero, SPDDebug.codex.Tricks.VisitShrineArray[Shrine.Index]).SetTarget(KingdomSquare);
                    }

                    if (KingdomSquare.Character?.Resident?.Shop != null && KingdomSquare.Fixture?.Container != null)
                    {
                        // all shops are randomised.
                        var Shop = NextRandomShop();
                        if (Shop != null)
                        {
                            // replace the shop.
                            KingdomSquare.Character.SetResidentShop(KingdomSquare, Shop);

                            // force a shop zone.
                            var ShopZone = KingdomMap.AddZone();
                            ShopZone.ForceRegion(KingdomSquare.FindBoundary());

                            var ShopStall = KingdomSquare.Fixture.Container;

                            ShopStall.Stash.RemoveAll(); // delete what build map produced.

                            if (StandardGeneration)
                            {
                                // visit trigger.
                                ShopZone.InsertTrigger().AddSchedule(Delay.Zero, SPDDebug.codex.Tricks.VisitShopArray[Shop.Index]).SetTarget(KingdomSquare);

                                // the shop is fully stocked.
                                SPDDebug.generator.StockShop(KingdomSquare, ShopStall, Shop, (2.d4() + 4).Roll());
                            }
                            else
                            {
                                // kill the merchant and drop a corpse.
                                SPDDebug.generator.CorpseSquare(KingdomSquare);

                                // the shop is ransacked.
                                SPDDebug.generator.StockShop(KingdomSquare, ShopStall, Shop, (1.d3() + 2).Roll());
                            }
                        }
                    }
                    else if (KingdomSquare.Fixture?.Feature == SPDDebug.codex.Features.stall)
                    {
                        // abandoned shop, no keeper, but some left over stock, unrelated to the nearby shop.
                        SPDDebug.generator.StockShop(KingdomSquare, KingdomSquare.Fixture.Container, NextRandomShop(), (1.d3() + 2).Roll());
                    }

                    if (!StandardGeneration && KingdomSquare.Character?.Entity == SPDDebug.codex.Entities.high_elf)
                    {
                        if (IsTown)
                            SPDDebug.generator.CorpseSquare(KingdomSquare); // murder all high elves in the town
                        else
                            KingdomSquare.Character.SetNeutral(false); // high elves are hostile in the palace.
                    }

                    if (!StandardGeneration && KingdomSquare.Character != null)
                    {
                        var UpgradeCharacter = KingdomSquare.Character;

                        var Skills = SPDDebug.codex.Skills;
                        var Qualifications = SPDDebug.codex.Qualifications;

                        if (UpgradeCharacter.Entity == SPDDebug.codex.Entities.orc_king)
                        {
                            var Elements = SPDDebug.codex.Elements;

                            UpgradeCharacter.SetResistance(Elements.cold, 100);
                            UpgradeCharacter.SetResistance(Elements.fire, 100);
                            UpgradeCharacter.SetResistance(Elements.magical, 100);
                            UpgradeCharacter.SetResistance(Elements.poison, 100);
                        }
                        else if (UpgradeCharacter.Entity == SPDDebug.codex.Entities.orc_captain)
                        {
                            UpgradeCharacter.ForceCompetency(Skills.light_blade).Set(Qualifications.master);
                            UpgradeCharacter.ForceCompetency(Skills.medium_blade).Set(Qualifications.master);
                            UpgradeCharacter.ForceCompetency(Skills.heavy_blade).Set(Qualifications.master);
                            UpgradeCharacter.ForceCompetency(Skills.light_armour).Set(Qualifications.master);
                            UpgradeCharacter.ForceCompetency(Skills.medium_armour).Set(Qualifications.master);
                            UpgradeCharacter.ForceCompetency(Skills.heavy_armour).Set(Qualifications.master);
                        }
                        else if (UpgradeCharacter.Entity == SPDDebug.codex.Entities.orc_shaman)
                        {
                            UpgradeCharacter.ForceCompetency(Skills.evocation).Set(Qualifications.specialist);
                        }
                    }

                    if (KingdomSquare.Fixture?.Feature == SPDDebug.codex.Features.throne && KingdomSquare.Character?.Entity == SPDDebug.codex.Entities.elf_king)
                    {
                        // the elf king is powerful.
                        var KingCharacter = KingdomSquare.Character;

                        SPDDebug.generator.PromoteCharacter(KingCharacter, KingdomMap.Difficulty - KingCharacter.Level + 5);

                        var Properties = SPDDebug.codex.Properties;
                        var Elements = SPDDebug.codex.Elements;

                        KingCharacter.AcquireTalent(Properties.free_action, Properties.invisibility, Properties.polymorph_control, Properties.slippery);
                        KingCharacter.SetResistance(Elements.cold, 100);
                        KingCharacter.SetResistance(Elements.fire, 100);
                        KingCharacter.SetResistance(Elements.magical, 100);
                        KingCharacter.SetResistance(Elements.poison, 100);

                        KingCharacter.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(KingdomSquare, SPDDebug.codex.Items.potion_of_full_healing, 1));
                        KingCharacter.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(KingdomSquare, SPDDebug.codex.Items.potion_of_extra_healing, 3));
                        KingCharacter.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(KingdomSquare, SPDDebug.codex.Items.scroll_of_terror, 2));
                        KingCharacter.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(KingdomSquare, SPDDebug.codex.Items.scroll_of_confusion, 2));
                        KingCharacter.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(KingdomSquare, SPDDebug.codex.Items.potion_of_blindness, 1));
                        KingCharacter.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(KingdomSquare, SPDDebug.codex.Items.potion_of_speed, 1));
                        KingCharacter.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(KingdomSquare, SPDDebug.codex.Items.potion_of_sleeping, 1));
                        KingCharacter.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(KingdomSquare, SPDDebug.codex.Items.potion_of_sickness, 1));

                        // maximum knowledge of the known spells.
                        var SpellArray = new[]
                        {
                            SPDDebug.codex.Spells.drain_life,
                            SPDDebug.codex.Spells.cone_of_cold,
                            SPDDebug.codex.Spells.poison_blast,
                            SPDDebug.codex.Spells.ice_storm
                        };

                        foreach (var Spell in SpellArray)
                            KingCharacter.Knowledge.LearnSpell(Spell, 4);

                        // master competency in all skills.
                        foreach (var Competency in KingCharacter.Competencies)
                            Competency.Set(SPDDebug.codex.Qualifications.master);

                        var SchoolSkillArray = SpellArray.Select(S => S.School.Skill).Distinct().ToArray();
                        foreach (var SchoolSkill in SchoolSkillArray.Except(KingCharacter.Competencies.Select(C => C.Skill)))
                            KingCharacter.AddCompetency(SchoolSkill, SPDDebug.codex.Qualifications.master);

                        if (!StandardGeneration)
                            SPDDebug.generator.AcquireArtifact(KingdomSquare, KingCharacter, SPDDebug.codex.Qualifications.master);
                    }

                    if (IsTown && KingdomSquare.Character != null && KingdomSquare.Character.Neutral)
                    {
                        // all neutrals in town are allied together.
                        TownParty.AddAlly(KingdomSquare.Character, Clock.Zero, Delay.Zero);
                    }

                    if (KingdomSquare.Character?.Entity == SPDDebug.codex.Entities.straw_golem)
                    {
                        // paralysed for training students.
                        KingdomSquare.Character.AcquireTalent(SPDDebug.codex.Properties.paralysis);
                    }

                    foreach (var Asset in KingdomSquare.GetAssets())
                    {
                        if (Asset.Item == SPDDebug.codex.Items.sandwich)
                        {
                            // sandwich is replaced by an artifact.
                            KingdomSquare.RemoveAsset(Asset);

                            if (StandardGeneration)
                            {
                                var KingItem = SPDDebug.generator.GetArtifactItem();
                                if (KingItem != null)
                                    SPDDebug.generator.PlaceSpecificAsset(KingdomSquare, KingItem);
                            }
                        }
                        else if (Asset.Container != null)
                        {
                            // random container loot.
                            if (Asset.Item.Type == ItemType.Chest && Asset.Container.Stash.Count == 0)
                                SetContainer(KingdomSquare, Asset.Item.Storage, Asset.Container, Locked: true, Trapped: true);
                        }
                        else if (Asset.Item != SPDDebug.codex.Items.skeleton_key && Asset.Item.Type != ItemType.Corpse)
                        {
                            // randomise items.
                            KingdomSquare.RemoveAsset(Asset);
                            var ReplaceAsset = SPDDebug.generator.NewRandomAsset(KingdomSquare, Asset.Item.Stock);
                            if (Asset.Quantity > 1)
                                ReplaceAsset.SetQuantity(Asset.Quantity);
                            KingdomSquare.PlaceAsset(ReplaceAsset);
                        }
                    }
                }
            }

            foreach (var QuestLevel in QuestSite.Levels)
            {
                var KingdomMap = QuestLevel.Map;
                KingdomMap.SetDifficulty(KingdomDifficulty + QuestLevel.Index);
                KingdomMap.SetAtmosphere(SPDDebug.codex.Atmospheres.forest);
                KingdomMap.SetTerminal(QuestLevel == QuestSite.LastLevel);

                SPDDebug.generator.Adventure.World.AddMap(KingdomMap);

                var KingdomLevel = KingdomSite.AddLevel(QuestLevel.Index, KingdomMap);

                GenerateMap(KingdomMap);
                if (KingdomLevel.Index == 1) KingdomLevel.SetTransitions(InnerPortalSquare, QuestLevel.DownSquare);
                else if (KingdomLevel.Index == 4) KingdomLevel.SetTransitions(QuestLevel.UpSquare, QuestStart);
                else KingdomLevel.SetTransitions(QuestLevel.UpSquare, QuestLevel.DownSquare);

                foreach (var QuestMap in QuestLevel.GetMaps().Except(KingdomMap))
                {
                    QuestMap.SetDifficulty(KingdomMap.Difficulty);
                    QuestMap.SetAtmosphere(SPDDebug.codex.Atmospheres.civilisation);

                    SPDDebug.generator.Adventure.World.AddMap(QuestMap);

                    QuestMap.SetLevel(KingdomLevel);

                    GenerateMap(QuestMap);
                    if (KingdomLevel.Index == 1) KingdomLevel.SetTransitions(InnerPortalSquare, QuestLevel.DownSquare);
                    else if (KingdomLevel.Index == 4) KingdomLevel.SetTransitions(QuestLevel.UpSquare, QuestStart);
                    else KingdomLevel.SetTransitions(QuestLevel.UpSquare, QuestLevel.DownSquare);
                }
            }
        }
        private void CreateLichTowerBranch()
        {
            var Quest = SPDDebug.generator.ImportQuest(Resources.Quests.Tower.GetBuffer());
            var QuestSite = Quest.World.Sites.Single();
            var QuestStart = Quest.World.Start;
            BranchStartSquare = QuestStart;

            var TowerSite = SPDDebug.generator.Adventure.World.AddSite("Lich Tower");
            SPDDebug.currentBranchSite = TowerSite;

            var TowerDifficulty = SPDDebug.currentmap.depth;
            SetDifficultyGroup();

            foreach (var QuestLevel in QuestSite.Levels)
            {
                var TowerMap = QuestLevel.Map;
                TowerMap.SetName("Lich Tower " + QuestLevel.Index);
                TowerMap.SetDifficulty(TowerDifficulty + (QuestSite.Levels.Count - QuestLevel.Index));
                TowerMap.SetAtmosphere(SPDDebug.codex.Atmospheres.dungeon);

                SPDDebug.generator.Adventure.World.AddMap(TowerMap);

                var TowerLevel = TowerSite.AddLevel(QuestLevel.Index, TowerMap);
                TowerLevel.SetTransitions(QuestLevel.UpSquare, QuestLevel.DownSquare);

                SPDDebug.generator.BuildMap(TowerMap);

                foreach (var TowerSquare in TowerMap.GetSquares())
                {
                    if (TowerSquare.Zone != null)
                        TowerSquare.Zone.SetRestricted(true);

                    if (TowerSquare.Wall != null && TowerSquare.Wall.IsPhysical())
                        TowerSquare.Wall.SetStructure(WallStructure.Permanent);

                    if (TowerSquare.Passage != null && TowerSquare.Passage.Destination == null && TowerSquare != QuestStart)
                        BranchEndSquare = TowerSquare; // exit portal.

                    foreach (var Asset in TowerSquare.GetAssets())
                    {
                        if (Asset.Container != null)
                        {
                            // random container loot.
                            if (Asset.Item.Type == ItemType.Chest && Asset.Container.Stash.Count == 0)
                            {
                                SetContainer(TowerSquare, Asset.Item.Storage, Asset.Container, Locked: true, Trapped: true);

                                // every chest contains a book - magical knowledge and power is the treasure.
                                Asset.Container.Stash.Add(SPDDebug.generator.NewRandomAsset(TowerSquare, SPDDebug.codex.Stocks.book));
                            }
                        }
                        else
                        {
                            // randomise items.
                            TowerSquare.RemoveAsset(Asset);
                            SPDDebug.generator.PlaceRandomAsset(TowerSquare, Asset.Item.Stock);
                        }
                    }

                    var TowerCharacter = TowerSquare.Character;
                    if (TowerCharacter != null && TowerCharacter.Entity == SPDDebug.codex.Entities.dracolich)
                    {
                        SPDDebug.generator.PromoteCharacter(TowerCharacter, TowerDifficulty - TowerCharacter.Level + 5);

                        var Properties = SPDDebug.codex.Properties;
                        var Elements = SPDDebug.codex.Elements;

                        TowerCharacter.AcquireTalent(Properties.free_action, Properties.displacement, Properties.polymorph_control, Properties.slippery);
                        TowerCharacter.SetResistance(Elements.fire, 100);

                        // maximum knowledge of the known spells.
                        var SpellArray = new[]
                        {
                            SPDDebug.codex.Spells.drain_life,
                            SPDDebug.codex.Spells.haste,
                            SPDDebug.codex.Spells.invisibility,
                            SPDDebug.codex.Spells.summoning,
                            SPDDebug.codex.Spells.lightning_bolt,
                            SPDDebug.codex.Spells.cone_of_cold,
                            SPDDebug.codex.Spells.fireball,
                            SPDDebug.codex.Spells.ice_storm,
                            SPDDebug.codex.Spells.magic_missile
                        };

                        foreach (var Spell in SpellArray)
                            TowerCharacter.Knowledge.LearnSpell(Spell, 4);

                        // master competency in all skills.
                        foreach (var Competency in TowerCharacter.Competencies)
                            Competency.Set(SPDDebug.codex.Qualifications.master);

                        var SchoolSkillArray = SpellArray.Select(S => S.School.Skill).Distinct().ToArray();
                        foreach (var SchoolSkill in SchoolSkillArray.Except(TowerCharacter.Competencies.Select(C => C.Skill)))
                            TowerCharacter.AddCompetency(SchoolSkill, SPDDebug.codex.Qualifications.master);

                        // downgrade enchantment to specialist.
                        TowerCharacter.GetCompetency(SPDDebug.codex.Skills.enchantment).Set(SPDDebug.codex.Qualifications.specialist);

                        var ArtifactItem = SPDDebug.generator.GetArtifactItem();

                        if (ArtifactItem != null)
                        {
                            TowerCharacter.Inventory.Carried.Add(SPDDebug.generator.NewSpecificAsset(TowerSquare, ArtifactItem));

                            // dracolich will not use the artifact.
                            //SPDDebug.generator.OutfitCharacter(LairCharacter);
                        }
                    }
                }
            }
        }
        private void CreateAbyssBranch()
        {
            var Quest = SPDDebug.generator.ImportQuest(Resources.Quests.Abyss.GetBuffer());
            var QuestSite = Quest.World.Sites.Single();
            var QuestStart = Quest.World.Start;

            var AbyssSite = SPDDebug.generator.Adventure.World.AddSite("Abyss");

            var AbyssDifficulty = SPDDebug.currentmap.depth;
            SetDifficultyGroup();

            BranchStartSquare = QuestStart;

            void GenerateMap(Map Map)
            {
                Map.RotateRandomDegrees();

                SPDDebug.generator.BuildMap(Map);

                foreach (var AbyssSquare in Map.GetSquares())
                {
                    // allow teleporting in this branch.
                    //if (Square.Zone != null)
                    //  Square.Zone.SetRestricted(true);

                    if (AbyssSquare.Wall != null && AbyssSquare.Wall.IsPhysical())
                        AbyssSquare.Wall.SetStructure(WallStructure.Permanent);

                    if (AbyssSquare.Passage != null && AbyssSquare.Passage.Destination == null && AbyssSquare != QuestStart)
                        BranchEndSquare = AbyssSquare; // exit rift.

                    foreach (var Asset in AbyssSquare.GetAssets())
                    {
                        if (Asset.Container != null)
                        {
                            var ContainerItem = Asset.Item;

                            // random container loot.
                            if (ContainerItem.Type == ItemType.Chest && Asset.Container.Stash.Count == 0)
                            {
                                // randomise container appearance.
                                Asset.Convert(SPDDebug.generator.ContainerItems.GetRandom(), SPDDebug.codex.Sanctities.Uncursed);

                                var Trapped = Chance.OneIn3.Hit(); // 1 in 3 chests are trapped.
                                var Locked = Trapped || !Chance.OneIn3.Hit(); // all trapped chests are locked, 2 in 3 untrapped chests are also locked.
                                SetContainer(AbyssSquare, Asset.Item.Storage, Asset.Container, Locked, Trapped);

                                // chests are considered 'rich' and are double-stocked with items.
                                if (ContainerItem == SPDDebug.codex.Items.chest)
                                    StockContainer(AbyssSquare, Asset.Item.Storage, Asset.Container);
                            }
                        }
                        else
                        {
                            // randomise items.
                            AbyssSquare.RemoveAsset(Asset);
                            SPDDebug.generator.PlaceRandomAsset(AbyssSquare, Asset.Item.Stock);
                        }
                    }

                    var AbyssCharacter = AbyssSquare.Character;
                    if (AbyssCharacter != null)
                    {
                        var Properties = SPDDebug.codex.Properties;
                        var Elements = SPDDebug.codex.Elements;

                        // minions.
                        AbyssCharacter.SetResistance(Elements.drain, 100);
                        AbyssCharacter.SetResistance(Elements.fire, 100);
                        AbyssCharacter.SetResistance(Elements.poison, 100);

                        var IsBoss = AbyssCharacter.Entity == SPDDebug.codex.Entities.nabassu;

                        if (AbyssCharacter.HasAcquiredTalent(Properties.sleeping) || IsBoss)
                        {
                            // flunkies.
                            AbyssCharacter.ReleaseTalent(Properties.sleeping);

                            AbyssCharacter.SetResistance(Elements.magical, 100);
                            AbyssCharacter.SetResistance(Elements.sleep, 100);
                            AbyssCharacter.SetResistance(Elements.petrify, 100);

                            if (!AbyssCharacter.HasAcquiredTalent(Properties.polymorph_control))
                                AbyssCharacter.AcquireTalent(Properties.polymorph_control);

                            if (!AbyssCharacter.HasAcquiredTalent(Properties.slippery))
                                AbyssCharacter.AcquireTalent(Properties.slippery);

                            if (IsBoss)
                            {
                                // the boss.

                                // resistances to all main elements.
                                foreach (var Element in Elements.List.Where(E => E.IsResistance))
                                    AbyssCharacter.SetResistance(Element, 100);

                                AbyssCharacter.AcquireTalent(Properties.reflection, Properties.see_invisible, Properties.teleport_control, Properties.vitality);

                                Asset NewAsset(Item Item, int Quantity, Sanctity Sanctity)
                                {
                                    var Result = SPDDebug.generator.NewSpecificAsset(AbyssSquare, Item, Quantity);
                                    Result.SetSanctity(Sanctity);
                                    return Result;
                                }
                                AbyssCharacter.Inventory.Carried.Add(NewAsset(SPDDebug.codex.Items.scroll_of_terror, 1, SPDDebug.codex.Sanctities.Blessed));
                                AbyssCharacter.Inventory.Carried.Add(NewAsset(SPDDebug.codex.Items.scroll_of_teleportation, 1, SPDDebug.codex.Sanctities.Uncursed));
                                AbyssCharacter.Inventory.Carried.Add(NewAsset(SPDDebug.codex.Items.potion_of_full_healing, 2, SPDDebug.codex.Sanctities.Blessed));
                                AbyssCharacter.Inventory.Carried.Add(NewAsset(SPDDebug.codex.Items.potion_of_sickness, 1, SPDDebug.codex.Sanctities.Cursed));

                                SPDDebug.generator.AcquireArtifact(AbyssSquare, AbyssCharacter, SPDDebug.codex.Qualifications.master);
                            }
                        }
                    }
                }
            }

            foreach (var QuestLevel in QuestSite.Levels)
            {
                var AbyssMap = QuestLevel.Map;
                AbyssMap.SetName(SPDDebug.generator.EscapeTranslatedName(AbyssMap.Name));
                AbyssMap.SetDifficulty(AbyssDifficulty + (QuestSite.Levels.Count - QuestLevel.Index));
                AbyssMap.SetAtmosphere(SPDDebug.codex.Atmospheres.nether);
                AbyssMap.SetTerminal(QuestLevel == QuestSite.LastLevel);

                SPDDebug.generator.Adventure.World.AddMap(AbyssMap);

                var AbyssLevel = AbyssSite.AddLevel(QuestLevel.Index, AbyssMap);
                AbyssLevel.SetTransitions(QuestLevel.UpSquare, QuestLevel.DownSquare);

                GenerateMap(AbyssMap);

                foreach (var QuestMap in QuestLevel.GetMaps().Except(AbyssMap))
                {
                    QuestMap.SetName(SPDDebug.generator.EscapeTranslatedName(QuestMap.Name));
                    QuestMap.SetDifficulty(AbyssMap.Difficulty);
                    QuestMap.SetAtmosphere(AbyssMap.Atmosphere);
                    QuestMap.SetTerminal(AbyssMap.Terminal);

                    SPDDebug.generator.Adventure.World.AddMap(QuestMap);

                    QuestMap.SetLevel(AbyssLevel);

                    GenerateMap(QuestMap);
                }
            }
        }
        private void CreateMarketBranch()
        {
            var Quest = SPDDebug.generator.ImportQuest(Resources.Quests.Market.GetBuffer());
            var QuestSite = Quest.World.Sites.Single();
            SPDDebug.currentBranchSite = QuestSite;
            var QuestStart = Quest.World.Start;

            var MarketSite = SPDDebug.generator.Adventure.World.AddSite("Market");

            var MarketDifficulty = SPDDebug.currentmap.depth;
            SetDifficultyGroup();

            var MercenaryProbability = SPDDebug.codex.Entities.List.Where(E => E.IsMercenary && E.IsEncounter).ToProbability(E => E.Level); // higher level ones are preferred.

            var ShopList = SPDDebug.codex.Shops.List.Where(S => !SPDDebug.generator.Adventure.Abolition || !S.SellsOnlyAbolitionCandidates()).ToDistinctList();

            Shop GetShop()
            {
                if (ShopList.Count == 0)
                    ShopList.AddRange(SPDDebug.codex.Shops.List.Where(S => !SPDDebug.generator.Adventure.Abolition || !S.SellsOnlyAbolitionCandidates()));

                var Result = ShopList.GetRandom();
                ShopList.Remove(Result);
                return Result;
            }

            var ItemDice = 2.d4() + 6;

            var QuestLevel = QuestSite.Levels.Single();

            var MarketMap = QuestLevel.Map;
            MarketMap.SetName("Market");
            MarketMap.SetDifficulty(MarketDifficulty + QuestLevel.Index);
            MarketMap.SetAtmosphere(SPDDebug.codex.Atmospheres.civilisation);
            MarketMap.SetTerminal(true);

            SPDDebug.generator.Adventure.World.AddMap(MarketMap);

            var MarketLevel = MarketSite.AddLevel(QuestLevel.Index, MarketMap);
            MarketLevel.SetTransitions(QuestLevel.UpSquare, QuestLevel.DownSquare);

            SPDDebug.generator.BuildMap(MarketMap);

            var Party = SPDDebug.generator.NewParty(Leader: null);

            foreach (var MarketSquare in MarketMap.GetSquares().Where(S => !S.IsEmpty()))
            {
                MarketSquare.SetLit(true);

                // no teleport.
                if (MarketSquare.Zone != null)
                    MarketSquare.Zone.SetRestricted(true);

                // permanent walls.
                if (MarketSquare.Wall != null)
                    MarketSquare.Wall.SetStructure(WallStructure.Permanent);

                // return portal.
                if (MarketSquare.Passage != null)
                {
                    BranchStartSquare = MarketSquare;
                    BranchEndSquare = MarketSquare;
                }

                // statues are black marketeers as well.
                if (MarketSquare.Boulder != null && MarketSquare.Boulder.Block.Prison)
                    MarketSquare.Boulder.SetPrisoner(SPDDebug.generator.NewCharacter(MarketSquare, MercenaryProbability.GetRandomOrNull()));

                // create a shop.
                if (MarketSquare.Fixture != null && MarketSquare.Fixture.Feature == SPDDebug.codex.Features.stall)
                    SPDDebug.generator.PlaceShop(MarketSquare, GetShop(), ItemDice.Roll());

                // all neutrals are allied.
                var Character = MarketSquare.Character;
                if (Character != null && Character.Neutral)
                {
                    Party.AddAlly(Character, Clock.Zero, Delay.Zero);

                    // guards are resident.
                    if (Character.Entity == SPDDebug.codex.Entities.guard && !Character.IsResident())
                        Character.SetResidentSquare(MarketSquare);
                }

                // randomise items.
                foreach (var Asset in MarketSquare.GetAssets())
                {
                    if (Asset.Container != null)
                    {
                        // random container loot.
                        if (Asset.Item.Type == ItemType.Chest && Asset.Container.Stash.Count == 0)
                        {
                            SetContainer(MarketSquare, Asset.Item.Storage, Asset.Container, Locked: true, Trapped: true);

                            //Asset.Container.Stash.Add(CreateCoins(MarketSquare, 1000.d10().Roll())); // 1000-10,000 gold.
                        }
                    }
                    else
                    {
                        // randomise items.
                        MarketSquare.RemoveAsset(Asset);
                        SPDDebug.generator.PlaceRandomAsset(MarketSquare, Asset.Item.Stock);
                    }
                }
            }
        }
        private void SetDifficultyGroup()
        {
            var Depth = SPDDebug.currentmap.depth;
            if (Depth < 5) DifficultyGroup = 1;
            if (Depth == 5) DifficultyGroup = 2;
            if (Depth > 5 && Depth < 10) DifficultyGroup = 3;
            if (Depth == 10) DifficultyGroup = 4;
            if (Depth > 10 && Depth < 15) DifficultyGroup = 5;
            if (Depth == 15) DifficultyGroup = 6;
            if (Depth > 15 && Depth < 20) DifficultyGroup = 7;
            if (Depth == 20) DifficultyGroup = 8;
            if (Depth > 20 && Depth < 25) DifficultyGroup = 9;
            if (Depth == 25) DifficultyGroup = 10;
        }
        private void RecruitParty(Square Square, Party Party)
        {
            var Character = Square.Character;
            if (Character != null)
            {
                Character.SetNeutral(true);
                Party.AddAlly(Character, Clock.Zero, Delay.Zero);
            }
        }
        private void CreateMazePaths(Map Map, Region Region)
        {
            var SquareStack = new Stack<Square>();

            var StartColumn = (1.d((Region.Width - 1) / 2).Roll() - 1) * 2 + 1;
            var StartRow = (1.d((Region.Height - 1) / 2).Roll() - 1) * 2 + 1;

            var NextSquare = Map[Region.Left + StartColumn, Region.Top + StartRow];
            SquareStack.Push(NextSquare);

            while (SquareStack.Count > 0)
            {
                var NeighbourArray = NextSquare.GetNeighbourSquares().Where(S => S.Wall != null && !S.IsEdge(Region)).ToArray();
                if (NeighbourArray.Length > 0)
                {
                    var Neighbour = NeighbourArray.GetRandom();
                    var NeighbourUnderlay = Neighbour.Wall.Barrier.Underlay;
                    Neighbour.SetWall(null);
                    SPDDebug.generator.PlaceFloor(Neighbour, NeighbourUnderlay);

                    var Between = NextSquare.Adjacent(NextSquare.AsDirection(Neighbour));
                    if (Between.Wall != null)
                    {
                        var BetweenUnderlay = Between.Wall.Barrier.Underlay;
                        Between.SetWall(null);
                        SPDDebug.generator.PlaceFloor(Between, BetweenUnderlay);
                    }

                    SquareStack.Push(NextSquare);
                    NextSquare = Neighbour;
                }
                else
                {
                    NextSquare = SquareStack.Pop();
                }
            }
        }
        private void CreateMazeDetails(Map Map, Block Block, Region Region, int BoxSize)
        {
            var BoxWidth = Region.Width / BoxSize;
            var BoxHeight = Region.Height / BoxSize;

            var BoxTop = Region.Top;

            for (var BoxY = 0; BoxY <= BoxHeight; BoxY++)
            {
                if (BoxTop > Region.Bottom)
                    break;

                var BoxBottom = Math.Min(Region.Bottom, BoxTop + BoxSize - 1);

                var BoxLeft = Region.Left;

                for (var BoxX = 0; BoxX <= BoxWidth; BoxX++)
                {
                    if (BoxLeft > Region.Right)
                        break;

                    var BoxRight = Math.Min(Region.Right, BoxLeft + BoxSize - 1);

                    //CreateRoomDetails(Map, Block, Map.GetSquares(new Region(BoxLeft, BoxTop, BoxRight, BoxBottom)).Where(S => S.Wall == null).ToDistinctList());

                    BoxLeft += BoxSize;
                }

                BoxTop += BoxSize;
            }
        }
        private void SetContainer(Square Square, Storage Storage, Container Container, bool Locked, bool Trapped)
        {
            Container.Locked = Storage.Locking && Locked;
            Container.Trapped = Storage.Trapping && Trapped;
        }
        private void StockContainer(Square Square, Storage Storage, Container Container)
        {
            foreach (var _ in Storage.ContainedDice.Roll().NumberSeries())
            {
                var ContainedAsset = SPDDebug.generator.NewRandomAsset(Square, Stock: null);

                // don't allow container inside container.
                var Attempt = 0;
                while (ContainedAsset.Container != null && Attempt < 10)
                {
                    ContainedAsset = SPDDebug.generator.NewRandomAsset(Square, Stock: null);
                    Attempt++;
                }

                if (ContainedAsset.Container == null)
                    Container.Stash.Add(ContainedAsset);
            }
        }
        private void CreateLevelOneMonster(Square DestSquare)
        {
            switch (SPDRandom.Int(7))
            {
                case 0:
                    PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.blob);
                    break;
                case 1:
                    PlaceCharacter(DestSquare, 1, 30, E => E.Kind == SPDDebug.codex.Kinds.eye);
                    break;
                case 2:
                    PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.fungus);
                    break;
                case 3:
                    PlaceCharacter(DestSquare, 1, 29, E => E.Kind == SPDDebug.codex.Kinds.pudding);
                    break;
                case 4:
                    PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.light);
                    break;
                case 5:
                    PlaceCharacter(DestSquare, 1, 13, E => E.Kind == SPDDebug.codex.Kinds.elemental && E != SPDDebug.codex.Entities.stalker);
                    break;
                case 6:
                    PlaceCharacter(DestSquare, 1, 40, E => E.Kind == SPDDebug.codex.Kinds.jelly);
                    break;
                default: break;
            }
        }
        private Entity RandomEntity(int MinimumDifficulty, int MaximumDifficulty, Func<Entity, bool> FilterFunc, bool ExceptExtinct = true)
        {
            IEnumerable<Entity> EntityQuery = from E in SPDDebug.codex.Entities.List
                                              where E.IsEncounter && E.Frequency > 0 && E.Difficulty >= MinimumDifficulty && E.Difficulty <= MaximumDifficulty && (FilterFunc == null || FilterFunc(E))
                                              select E;
            if (ExceptExtinct)
            {
                EntityQuery = EntityQuery.Except(SPDDebug.adventure.World.GetExtinctEntities());
            }
            return EntityQuery.GetRandomOrNull();
        }
        private void SnoozeCharacter(Character Character)
        {
            if (Inv.Assert.IsEnabled)
                Inv.Assert.CheckNotNull(Character, nameof(Character));
            
            if (!Character.HasCompleteResistance(SPDDebug.codex.Elements.sleep))
                Character.AcquireTalent(SPDDebug.codex.Properties.sleeping);
        }

        private void PlaceCharacter(Square Square, int MinimumDifficulty, int MaximumDifficulty, Func<Entity, bool> FilterFunc)
        {
            Func<Entity, bool> TerrainFunc;
            if (Square.IsSunken() && Square.Boulder == null)
            {
                TerrainFunc = (Entity E) => E.HasOnlyTerrain(Square.Floor.Ground.Substance);
            }
            else
            {
                TerrainFunc = (Entity E) => E.HasTerrain(SPDDebug.codex.Materials.air);
            }
            Entity Entity = RandomEntity(MinimumDifficulty, MaximumDifficulty, (Entity E) => (FilterFunc == null || FilterFunc(E)) && TerrainFunc(E), true);
            if (Entity != null)
            {
                SPDDebug.generator.PlaceCharacter(Square, Entity);
            }
        }
        private void PlaceShop(Square square, bool Orcs)
        {
            var shop = SPDDebug.codex.Shops.general_store;
            SPDDebug.generator.PlaceFixture(square, SPDDebug.codex.Features.stall);
            var stall = square.Fixture.Container;
            SPDDebug.generator.PlaceCharacter(square, SPDDebug.codex.Entities.merchant);
            var shopkeeper = square.Character;
            shopkeeper.Inventory.GetEquippedAsset(SPDDebug.codex.Slots.purse).SetQuantity(1);
            shopkeeper.SetResidentShop(square, shop);
            shopkeeper.SetNeutral(true);
            shopkeeper.AddCompetency(SPDDebug.codex.Skills.bartering, SPDDebug.codex.Qualifications.champion);
            shopkeeper.SetName(SPDDebug.generator.GetCharacterName(shop.KeeperNames));

            void StallAdd(Item StallItem)
            {
                if (Orcs ? SPDRandom.Int(10) == 0 : SPDRandom.Int(6) == 0) stall.Stash.Add(SPDDebug.generator.NewSpecificAsset(square, StallItem));
            }

            StallAdd(SPDDebug.gamelist.RandomWand());
            StallAdd(SPDDebug.gamelist.RandomRing());
            StallAdd(SPDDebug.gamelist.RandomAmulet());
            StallAdd(SPDDebug.gamelist.RandomTool());
            StallAdd(SPDDebug.gamelist.RandomTool());
            StallAdd(SPDDebug.gamelist.RandomScroll());
            StallAdd(SPDDebug.gamelist.RandomScroll());
            StallAdd(SPDDebug.gamelist.RandomScroll());
            StallAdd(SPDDebug.gamelist.RandomPotion());
            StallAdd(SPDDebug.gamelist.RandomPotion());
            StallAdd(SPDDebug.gamelist.RandomPotion());
            if (SPDRandom.Int(5) == 0) stall.Stash.Add(SPDDebug.generator.NewSpecificAsset(square, SPDDebug.gamelist.BranchTreasure(DifficultyGroup)));

            if (square.Zone != null)
                square.Zone.InsertTrigger().AddSchedule(Delay.Zero, SPDDebug.codex.Tricks.VisitShopArray[shop.Index]).SetTarget(square);
        }
        public Square BranchStartSquare;
        public Square BranchEndSquare;
        private int DifficultyGroup = 0;
    }
}
